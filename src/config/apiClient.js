/**
 * @flow
 * @format
 */

import Config from 'react-native-config';
import Axios from 'axios';
// import AxiosLogger from 'axios-logger';

export const apiClients = Axios.create({
  baseURL: Config.API_URL,
  timeout: 10000,
  headers: {
    'Content-Type': 'application/json',
    Accept: 'application/json',
  },
});

// apiClients.interceptors.request.use(AxiosLogger.requestLogger);
// apiClients.interceptors.response.use(AxiosLogger.responseLogger);

// export default apiClients;

export const getCustomerAlternateAddress = async (cid) => {
  try {
    let data = JSON.stringify({
      type: 'getCustomerAlternateAddress',
      cid: cid,
      company_code: Config.COMPANY_CODE,
    });
    const response = await apiClients.post('/remote_service', data);

    console.log('alternate_address => \n' + response);
    return response.data;
  } catch (error) {
    throw new Error('Unable to fetch getCustomerAlternateAddress');
  }
};
