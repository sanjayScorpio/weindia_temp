/**
 * @format
 * @flow
 */

const APP_COLOR = {
  primary: '#555CC4',
  primaryBlue1: '#05BEFE',
  primaryBlue2: '#4481EB',
  primaryGreen1: '#268E4F',
  primaryGreen2: '#175F36',
  primary1: '#ff6a00',
  primaryLight: '#829BF8',
  primaryGradientStart: '#4f44b6',
  primaryGradientEnd: '#4f44b6',
  secondaryGradientStart: '#FF1358',
  secondaryGradientEnd: '#FF1358',
  profileGradientStart: '#54CBF6',
  profileGradientEnd: '#49D2D0',
  secondary: '#FF1358',
  hardButtonStart: '#F52721',
  hardButtonEnd: '#F55005',
  mediumButtonStart: '#2F5BE6',
  mediumButtonEnd: '#3FA5F7',
  easyButtonStart: '#23814E',
  easyButtonEnd: '#3ED183',
  grey: '#acacac',
  gray: '#5f5f5f',
  darkGray: '#4d4d4d',
  lightGray: '#9b9b9b',
  lightGray2: '#4c4c4c',
  white: '#ffffff',
  blue: '#5A81F7',
  bluish: '#F1F1F7',
  black: '#000000',
  green: '#6DD0A3',
  yellow: '#ffc247',
  red500: '#f44336',
  amber500: '#FFC107',
  green500: '#4CAF50',
  lightGray: '#9b9b9b',
  lightGray2: '#4c4c4c',
  white: '#ffffff',
  blue: '#5A81F7',
  bluish: '#F1F1F7',
  black: '#000000',
};

export default APP_COLOR;