/* eslint-disable no-underscore-dangle */
/**
 * @format
 * @flow
 */

import * as React from 'react';
import { StyleSheet, View, Dimensions, Image } from 'react-native';
import { ActivityIndicator, Text } from 'react-native-paper';
import { PrefUtils } from '../utils/PrefUtils';
import AsyncStorage from '@react-native-community/async-storage';
import { StackActions, NavigationActions } from 'react-navigation';
import LinearGradient from 'react-native-linear-gradient';
import AppIntroSlider from 'react-native-app-intro-slider';
import Walk1 from '../../assets/images/walk1.png';
import Walk2 from '../../assets/images/walk2.png';
import Walk3 from '../../assets/images/walk3.png';
import Colors from '../styles/colors';

const slides = [
  {
    key: 'somethun',
    text: 'Easy way to advertise',
    text2: 'within city.',
    image: Walk1,
    backgroundColor: Colors.darkGray,
  },
  {
    key: 'somethun-dos',
    text: 'Get the live tracking of',
    text2: 'advertising unit.',
    image: Walk2,
    backgroundColor: Colors.darkGray,
  },
  {
    key: 'somethun1',
    text: 'Moniter the units and places to',
    text2: 'cover the advertising\n of your venture.',
    image: Walk3,
    backgroundColor: Colors.darkGray,
  }
];

class Walkthrough extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showRealApp: false
    }
  }

  _onDone = async () => {
    const userData = await AsyncStorage.getItem(
      PrefUtils.PREF_USER_DATA,
    );
    console.log('userEmail  => ' + userData);
    // This will switch to the App screen or Auth screen and this loading
    // screen will be unmounted and thrown away.

    this.props.navigation.navigate(
      userData === null ? 'Auth' : 'App',
    );
  }

  _renderItem = ({ item }) => {
    return (
      <View>
        <View style={styles.container}>
          <View style={styles.slide}>
            <Image source={item.image} style={styles.imagesize} />
            <Text style={styles.text}>{item.text}</Text>
            <Text style={styles.text2}>{item.text2}</Text>
          </View>
        </View>
      </View>
    );
  }

  render() {
    return (
      <AppIntroSlider buttonTextStyle={{ color: Colors.darkGray }} activeDotStyle={{ backgroundColor: Colors.primaryBlue1, }} renderItem={this._renderItem} slides={slides} onDone={this._onDone} />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
    margin: 8,
  },
  linearGradient: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
  },
  slide: {
    alignSelf: 'center',
    justifyContent: 'center'
  },
  text: {
    fontSize: 24,
    color: Colors.darkGray,
    alignSelf: 'center',
    marginTop: 50,
    justifyContent: 'center',
    textAlign : 'center'
  },
  text2: {
    fontSize: 24,
    color: Colors.darkGray,
    alignSelf: 'center',
    justifyContent: 'center',
    textAlign : 'center'
  },
  imagesize: {
    height: 300,
    width: 350,
    alignSelf: 'center',
    margin : 20
  },
  slide: {
    marginTop: 80
  }
});

export default Walkthrough;
