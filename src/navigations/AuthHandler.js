/* eslint-disable no-underscore-dangle */
/**
 * @format
 * @flow
 */

import * as React from 'react';
import { StyleSheet, View, Dimensions, Image } from 'react-native';
import { ActivityIndicator } from 'react-native-paper';
import { PrefUtils } from '../utils/PrefUtils';
import AsyncStorage from '@react-native-community/async-storage';
import { StackActions, NavigationActions } from 'react-navigation';
import LinearGradient from 'react-native-linear-gradient';
import { Title } from 'react-native-paper';
import Colors from '../styles/colors'

class AuthHandler extends React.Component {
    constructor(props) {
        super(props);
        // noinspection JSIgnoredPromiseFromCall
        // this.navigateWithAuth();
    }

    componentDidMount() {
        setTimeout(() => {
            this.navigateWithAuth();
        }, 1800);
    }

    // Fetch the userId from storage then navigate to our appropriate place
    navigateWithAuth = async () => {
        const userData = await AsyncStorage.getItem(
            PrefUtils.PREF_USER_DATA,
        );
        console.log('userEmail  => ' + userData);
        // This will switch to the App screen or Auth screen and this loading
        // screen will be unmounted and thrown away.

        // this.props.navigation.navigate('Walkthrough');
        this.props.navigation.navigate('App');
    };

    render() {
        return (
            <View>
                    <View style = {{justifyContent : 'center', alignItems : 'center'}}>
                    <LinearGradient colors={[Colors.primaryGreen1, Colors.primaryGreen2]}
                    start={{x: 0, y: 0}} end={{x: 0, y: 1}}
                    >
                       <View style = {styles.container}>
                       {/* <Image
                            style={{
                                height: 120,
                                alignSelf: 'center',
                                width: 120,
                            }}
                            source={whitelogo}
                            resizeMode="center"
                        /> */}
                        <Title style={styles.logoText}>We India</Title>
                       </View>
                    </LinearGradient>
                    </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width : Dimensions.get('window').width,
        height : Dimensions.get('window').height,
        justifyContent : 'center',
        alignItems : 'center'
    },
    whitecolor: {
        color: '#ffffff'
    },
    imageContainer: {
        backgroundColor: '#ffffff',
        alignItems: 'center',
    },
    logoText: {
        padding: 10,
        fontSize: 36,
        color: 'white',
    },
});

export default AuthHandler;
