/**
 * @format
 * @flow
 */

import {createStackNavigator} from 'react-navigation-stack';
import MobileSignIn from '../screens/MobileSignIn';
import OtpVertfication from '../screens/OtpVertfication';

const AuthStack = createStackNavigator(
    {
        SignIn: {
            screen: MobileSignIn,
        },
        OtpVertfication : {
            screen : OtpVertfication
        },
    },
    {
        headerMode: 'none',
    },
);

export default AuthStack;
