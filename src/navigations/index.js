import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import AuthHandler from './AuthHandler';
import Auth from './AuthNavigator';
import App from './HomeNavigator';
// import Walkthrough from './walkthrough';
export default createAppContainer(
  createSwitchNavigator(
    {
      App,
      Auth,
      // Walkthrough,
      AuthHandler,
    },
    {
      initialRouteName: 'AuthHandler',
    },
  ),
);
