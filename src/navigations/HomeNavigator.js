import * as React from 'react';
import FeatherIcon from 'react-native-vector-icons/Feather';
import DashboardScreen from '../screens/DashboardScreen';
import SubCardDetailsScreen from '../screens/SubCardDetailsScreen';
import { createDrawerNavigator, DrawerItems } from 'react-navigation-drawer';
import SafeAreaView from 'react-native-safe-area-view';
import Colors from '../styles/colors';
import { Text, Button } from 'react-native-paper';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import LinearGradient from 'react-native-linear-gradient';
import { Header } from 'react-navigation-stack';
import Profile from '../screens/ProfileScreen'
import Settings from '../screens/Settings';
import CardBrowser from '../screens/CardBrowser';
import CustomPack from '../screens/CustomPack';
import UserManual from '../screens/UserManual';
import SendFeedBack from '../screens/SendFeedBack';
import Statistics from '../screens/Statistics';
import CardChapters from '../screens/cardChapters';
import { View, ScrollView, Dimensions, StyleSheet } from 'react-native';

const { width, height } = Dimensions.get('window');

//  let dialogvisible = false

//  const setDialogVisible = () => dialogvisible = !dialogvisible;


const CustomDrawerContentComponent = props => (
  <ScrollView>
    <SafeAreaView
      style={{ flex: 1 }}
      forceInset={{ top: 'always', horizontal: 'never' }}
    >
      <View style={{ width: width * 0.8, height: height * 0.2, }}>
        <LinearGradient
          colors={[Colors.primaryGreen1, Colors.green500]}
          start={{ x: 0, y: 0 }}
          end={{ x: 1, y: 1 }}
          style={{ ...StyleSheet.absoluteFillObject, alignItems: 'center', justifyContent: 'center' }}
        >
          {/* width: width * 0.8, height: height * 0.2 */}

          <Text style={{ fontSize: 30, color: Colors.white }}>We India</Text>
        </LinearGradient>
      </View>
      <DrawerItems
        activeTintColor={Colors.green500}
        inactiveTintColor={Colors.darkGray}
        
        // activeBackgroundColor = {Colors.green500}
        {...props} >
      </DrawerItems>
      <Button>Logout</Button>
    </SafeAreaView>
  </ScrollView>
);



const drawerStack = createDrawerNavigator(
  {
    Dashboard: {
      screen: CardChapters,
      navigationOptions: {
        drawerIcon: <FeatherIcon name="grid" size={20} />
      }
    },
    // screen: SubCardDetailsScreen,
    // navigationOptions: {
    //   headerTitleStyle: { color: '#fff' },
    //   header: props => <GradientHeader {...props} />,
    //   headerStyle: {
    //     backgroundColor: 'transparent',
    //     position: 'absolute',
    //     top: 0,
    //     left: 0,
    //     right: 0,
    //     bottom: 0,
    //   },
    // }
    // },
    SubCardDetails: {
      screen: SubCardDetailsScreen,
      navigationOptions: {
        drawerIcon: <FeatherIcon name="user" size={20} />
      }
    },
    'Card Browser': {
      screen: CardBrowser,
      navigationOptions: {
        drawerIcon: <FeatherIcon name="trello" size={20} />
      }
    },
    'Custom Pack': {
      screen: CustomPack,
      navigationOptions: {
        drawerIcon: <FeatherIcon name="package" size={20} />
      }
    },
    'Statistics': {
      screen: Statistics,
      navigationOptions: {
        drawerIcon: <FeatherIcon name="bar-chart-2" size={20} />
      }
    },
    'User Manual': {
      screen: UserManual,
      navigationOptions: {
        drawerIcon: <FeatherIcon name="book-open" size={20} />
      }
    },
    'Send Feedback': {
      screen: SendFeedBack,
      navigationOptions: {
        drawerIcon: <FeatherIcon name="message-square" size={20} />
      }
    },
    Settings: {
      screen: Settings,
      navigationOptions: {
        drawerIcon: <FeatherIcon name="settings" size={20} />
      }
    },
    Profile: {
      screen: Profile,
      navigationOptions: {
        drawerIcon: <FeatherIcon name="user" size={20} />
      }
    }
  }, {
  contentComponent: CustomDrawerContentComponent,
  drawerWidth: width * 0.8,
  
  // drawerBackgroundColor : Colors.green500,
})

const GradientHeader = props => (
  <View style={{ backgroundColor: '#eee' }}>
    <LinearGradient
      colors={[Colors.primaryBlue1, Colors.primaryBlue2]}
      start={{ x: 0, y: 0 }}
      end={{ x: 1, y: 1 }}
    >
      <Header {...props} style={{ backgroundColor: 'transparent' }} />
    </LinearGradient>
  </View>
)

const GradientHeader1 = props => (
  <View style={{ backgroundColor: '#eee' }}>
    <LinearGradient
      colors={[Colors.primaryBlue1, Colors.primaryBlue2]}
      start={{ x: 0, y: 0 }}
      end={{ x: 1, y: 1 }}
    >
      <Header {...props} style={{ backgroundColor: 'transparent' }} />
    </LinearGradient>
  </View>
)

export default drawerStack;
