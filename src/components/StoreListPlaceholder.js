import React, {Component} from 'react';

import {Alert, Clipboard, ScrollView, StyleSheet} from 'react-native';
import {Card} from 'react-native-paper';
import {Shine, Placeholder, PlaceholderLine, PlaceholderMedia} from 'rn-placeholder';

export default class StoreListPlaceholder extends Component {
    render() {
        return (
            <ScrollView style={styles.container}>
                <Card style={styles.cardContainer}>
                    <Placeholder
                        Left={PlaceholderMedia}
                        Animation={Shine}>
                        <PlaceholderLine/>
                        <PlaceholderLine width={80}/>
                        <PlaceholderLine width={50}/>
                        <PlaceholderLine width={60}/>
                    </Placeholder>
                </Card>
                <Card style={styles.cardContainer}>
                    <Placeholder
                        Left={PlaceholderMedia}
                        Animation={Shine}>
                        <PlaceholderLine/>
                        <PlaceholderLine width={80}/>
                        <PlaceholderLine width={50}/>
                        <PlaceholderLine width={60}/>
                    </Placeholder>
                </Card>
                <Card style={styles.cardContainer}>
                    <Placeholder
                        Left={PlaceholderMedia}
                        Animation={Shine}>
                        <PlaceholderLine/>
                        <PlaceholderLine width={80}/>
                        <PlaceholderLine width={50}/>
                        <PlaceholderLine width={60}/>
                    </Placeholder>
                </Card>
                <Card style={styles.cardContainer}>
                    <Placeholder
                        Left={PlaceholderMedia}
                        Animation={Shine}>
                        <PlaceholderLine/>
                        <PlaceholderLine width={80}/>
                        <PlaceholderLine width={50}/>
                        <PlaceholderLine width={60}/>
                    </Placeholder>
                </Card>
                <Card style={styles.cardContainer}>
                    <Placeholder
                        Left={PlaceholderMedia}
                        Animation={Shine}>
                        <PlaceholderLine/>
                        <PlaceholderLine width={80}/>
                        <PlaceholderLine width={50}/>
                        <PlaceholderLine width={60}/>
                    </Placeholder>
                </Card>
                <Card style={styles.cardContainer}>
                    <Placeholder
                        Left={PlaceholderMedia}
                        Animation={Shine}>
                        <PlaceholderLine/>
                        <PlaceholderLine width={80}/>
                        <PlaceholderLine width={50}/>
                        <PlaceholderLine width={60}/>
                    </Placeholder>
                </Card>
                <Card style={styles.cardContainer}>
                    <Placeholder
                        Left={PlaceholderMedia}
                        Animation={Shine}>
                        <PlaceholderLine/>
                        <PlaceholderLine width={80}/>
                        <PlaceholderLine width={50}/>
                        <PlaceholderLine width={60}/>
                    </Placeholder>
                </Card>
                <Card style={styles.cardContainer}>
                    <Placeholder
                        Left={PlaceholderMedia}
                        Animation={Shine}>
                        <PlaceholderLine/>
                        <PlaceholderLine width={80}/>
                        <PlaceholderLine width={50}/>
                        <PlaceholderLine width={60}/>
                    </Placeholder>
                </Card>
            </ScrollView>
        );
    }

    copy = content => () => {
        Clipboard.setString(content);
        Alert.alert('Copied!');
    };
}

const styles = StyleSheet.create({
    container: {
        padding: 16,
        marginVertical: 16,
        backgroundColor: '#efefef',
    },
    button: {
        marginTop: 20,
    },
    cardContainer: {
        padding: 8,
        marginTop: 8,
    },
});
