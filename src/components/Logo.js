/**
 * @flow
 * @format
 */
import * as React from 'react';
import {Image, StyleSheet, View} from 'react-native';
import {Title} from 'react-native-paper';
import  Colors  from '../styles/colors';

class Logo extends React.Component {
    render() {
        return (
            <View style={styles.imageContainer}>
                <Title style={styles.logoText}>We India</Title>
                <Title style={styles.logoText2}>Logo</Title>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    imageContainer: {
        alignItems: 'center',
        marginTop: 30,
        marginBottom: 30,
    },
    logoText: {
        fontSize: 26,
        color: Colors.primaryGreen1,
        fontWeight : 'normal'
    },
    logoText2: {
        fontSize: 18,
        color: Colors.primaryGreen2,
        fontWeight : 'normal'
    },
});

export default Logo;
