export const PrefUtils = {
    PREF_USER_DATA: "user_data",
    PREF_OFFICE_DATA:"office_data",
    PREF_SESSION_ID:"token_data",
    PREF_CURRENCY:"currency",
    PREF_WALKTHROUGH:"walkthrough",
    PREF_EXAMS: "exams",
    PREF_EXAMS_SUBJECTS: "subjects"
};