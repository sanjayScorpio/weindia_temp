import AsyncStorage from '@react-native-community/async-storage';

export const Capitalize = str => str.charAt(0).toUpperCase() + str.slice(1);

export const retrieveData = async key => {
  try {
    const value = await AsyncStorage.getItem(key);
    console.log(key + 'sd => ' + value);
    if (value !== null) {
      // We have data!!
      return value;
    }
  } catch (error) {
    // Error retrieving data
    console.log(error.message);
  }
};

export const storeData = async (key, value) => {
  try {
    await AsyncStorage.setItem(key, value);
  } catch (error) {
    // Error saving data
    console.log(error.message);
  }
};

export const removeData = async (key) => {
  try {
      const value = await AsyncStorage.removeItem(key);
      if (value !== null) {
          // We have data!!
          return value;
      }
  } catch (error) {
      // Error retrieving data
      console.log(error.message);
  }
};

export const log = (msg, color) => {
  color = color || 'black';
  let bgc = 'White';
  switch (color) {
    case 'success':
      color = 'Green';
      bgc = 'LimeGreen';
      break;
    case 'info':
      color = 'DodgerBlue';
      bgc = 'Turquoise';
      break;
    case 'error':
      color = 'Red';
      bgc = 'Black';
      break;
    case 'start':
      color = 'OliveDrab';
      bgc = 'PaleGreen';
      break;
    case 'warning':
      color = 'Tomato';
      bgc = 'Black';
      break;
    case 'end':
      color = 'Orchid';
      bgc = 'MediumVioletRed';
      break;
    default:
      break;
  }

  if (typeof msg === 'object') {
    console.log(msg);
  } else if (typeof color === 'object') {
    console.log(
      '%c' + msg,
      'color: PowderBlue;font-weight:bold; background-color: RoyalBlue;',
    );
    console.log(color);
  } else {
    console.log(
      '%c' + msg,
      'color:' + color + ';font-weight:bold; background-color: ' + bgc + ';',
    );
  }
};

export const getFileName = uri =>
  uri.substring(uri.lastIndexOf('/') + 1, uri.length);
