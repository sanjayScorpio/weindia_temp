import axios from 'axios';
import Config from 'react-native-config';
//import AxiosLogger from 'axios-logger';
import {log} from '../utils/CommonUtils';
/*
  Base client config for your application.
  Here you can define your base url, headers,
  timeouts and middleware used for each request.
*/
const client = axios.create({
    baseURL: Config.API_URL,
    timeout: 100000,
    headers: {
        'content-type': 'application/json',
        Accept: 'application/json',
    },
});
log(`API_URL -> ${Config.API_URL}`, 'info');
// client.interceptors.request.use(AxiosLogger.requestLogger, AxiosLogger.errorLogger);
// client.interceptors.response.use(AxiosLogger.responseLogger, AxiosLogger.errorLogger);

// Custom middleware for requests (this one just logs the error).
/*
client.interceptors.request.use(request => {
    const req = JSON.stringify(request);
    console.log("request =>",req);
    return Promise.resolve(request);
}, (error) => {
    console.log('Failed to make request with error:');
    console.log(error);
    return Promise.reject(error);
});
*/

// Custom middleware for responses (this one just logs the error).
/*client.interceptors.response.use(response => {
        const res = JSON.stringify(response);
        console.log("response => ", res);
        return Promise.resolve(response);
    },
    (error) => {
        console.log('Request got response with error:');
        console.log(error);
        return Promise.reject(error);
    });*/

export default client;
