import apiClient from './ApiClient';

class UserController {
    constructor() {}

    login = async (email, password) => {
        // Real implementation of a login request using the HttpClient
        try {
            let data = {
                username: email,
                password: password,
            };
            const res = await apiClient.post('/login', data);
            return res.data;
            // Data is the object exposes by axios for the response json
        } catch (error) {
            return error;
        }
        // This is a mocked example to simulate api behavior
        /*return new Promise((resolve, reject) => {
          if (email !== 'a@a.com' && password !== '') {
            setTimeout(
              () => resolve({ name: 'Jorge' }),
              1000,
            );
          } else {
            setTimeout(
              () => reject(new Error('Invalid Email/Password')),
              1000,
            );
          }
        });*/
    };

    logout = () => null;
}

export default new UserController();
