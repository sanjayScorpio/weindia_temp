/**
 * @flow
 * @format
 */
import * as React from 'react';
import { Alert, FlatList, Image, Linking, StyleSheet, View, Text, Dimensions } from 'react-native';
import Config from 'react-native-config';
import apiClient from '../controllers/ApiClient';
// import {} from 'react-navigation'
import { Appbar, Divider, List, Card, Paragraph, ThemeProvider } from 'react-native-paper';
import { findPlaceFromLatLng } from '../services/google.service';
import Permissions from 'react-native-permissions';
import { Capitalize } from '../utils/CommonUtils';
import Geolocation from '@react-native-community/geolocation';
import { DrawerActions } from 'react-navigation-drawer';
import FeatherIcon from 'react-native-vector-icons/Feather';
import { RNToasty } from 'react-native-toasty';
import StoreListPlaceholder from '../components/StoreListPlaceholder';
import Geocoder from "react-native-geocoding";
import LinearGradient from 'react-native-linear-gradient';
import Colors from '../styles/colors';

const { width, height } = Dimensions.get('window')

class DashboardScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      storeList: [],
      savedAddressList: [],
      results_found: 0,
      locationPermission: '',
      userData: '',
      formatted_address: '',
      short_address: '',
      refresh: false,
      isFetching: true,
    };

    // console.log("userData",this.userData);
  }

  // Check the status of a single permission
  componentDidMount() {
    // this.getCustomerAlternateAddress();
  }


  renderAppBar() {
    return (
      <Appbar.Header style={{ backgroundColor: Colors.white }}>
        <Appbar.Action icon={() => <FeatherIcon
          size={24}
          name="menu"
          style={{ backgroundColor: 'transparent' }}
          color={Colors.primaryGreen2}
        />}
          // onPress={() => { this.props.navigation.dispatch(DrawerActions.toggleDrawer())}} 
          onPress={() => this.props.navigation.openDrawer()}
        />
        <Appbar.Content title="We India" color={Colors.darkGray} />
        <Appbar.Action icon={() => <FeatherIcon
          size={24}
          name="search"
          style={{ backgroundColor: 'transparent' }}
          color={Colors.primaryGreen2}
        />} onPress={() => { this.props.navigation.goBack() }} />
        <Appbar.Action icon={() => <FeatherIcon
          size={24}
          name="more-vertical"
          style={{ backgroundColor: 'transparent' }}
          color={Colors.primaryGreen2}
        />} onPress={() => { this.props.navigation.goBack() }} />
      </Appbar.Header>
    );
  }


  renderStatusComponent() {
    return (
      <View style={{ justifyContent: 'space-between', backgroundColor: Colors.white, borderRadius: 10, borderColor: Colors.primaryGreen1, borderWidth: 2, width: width * 0.95, height: height * 0.2, elevation: 5 }}>
        <View style={{ flexDirection: 'row', alignItems: 'center', alignContent: 'center', paddingTop: 10 }}>
          <Text style={{ fontSize: 22, color: Colors.darkGray, paddingLeft: 15, paddingRight: 10 }}>Status</Text>
          <FeatherIcon name='info' size={20} style={{ backgroundColor: 'transparent' }} color={Colors.darkGray} />
        </View>
        
        <View style={{ flexDirection: 'row', justifyContent: 'space-evenly' }}>
          <View>
            <Text style={{ fontSize: 16, color: Colors.primaryGreen1, paddingLeft: 8, paddingTop: 5 }}>Today's status</Text>

            <View style={{ margin: 5, justifyContent: 'space-around', backgroundColor: Colors.white, borderRadius: 5, borderColor: Colors.primaryGreen1, borderWidth: 1, width: width * 0.42, height: height * 0.08 }}>
              <View style={{ flexDirection: 'row', padding: 2, justifyContent: 'space-between' }}>
                <Text style={{ fontSize: 12, color: Colors.primaryGreen1, padding: 5 }}>Card's Studied</Text>
                <Text style={{ fontSize: 12, color: Colors.darkGray, padding: 5 }}>21</Text>
              </View>
              <View style={{ flexDirection: 'row', padding: 2, justifyContent: 'space-between' }}>
                <Text style={{ fontSize: 12, color: Colors.primaryGreen1, padding: 5 }}>Total time spent</Text>
                <Text style={{ fontSize: 12, color: Colors.darkGray, padding: 5 }}>2153 m</Text>
              </View>
            </View>
          </View>
          <View>
            <Text style={{ fontSize: 16, color: Colors.primaryGreen1, paddingLeft: 8, paddingTop: 5 }}>Overall status</Text>

            <View style={{ margin: 5, justifyContent: 'space-around', backgroundColor: Colors.white, borderRadius: 5, borderColor: Colors.primaryGreen1, borderWidth: 1, width: width * 0.42, height: height * 0.08 }}>
              <View style={{ flexDirection: 'row', padding: 2, justifyContent: 'space-between' }}>
                <Text style={{ fontSize: 12, color: Colors.primaryGreen1, padding: 5 }}>Card's due</Text>
                <Text style={{ fontSize: 12, color: Colors.darkGray, padding: 5 }}>215</Text>
              </View>
              <View style={{ flexDirection: 'row', padding: 2, justifyContent: 'space-between' }}>
                <Text style={{ fontSize: 12, color: Colors.primaryGreen1, padding: 5 }}>Time left</Text>
                <Text style={{ fontSize: 12, color: Colors.darkGray, padding: 5 }}>25613 m</Text>
              </View>
            </View>
          </View>
        </View>
        <View>

        </View>
      </View>
    )
  }


  render() {
    return (
      <View>
        {this.renderAppBar()}
        <View style={styles.container}>
          {this.renderStatusComponent()}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    justifyContent: 'center',
    flexDirection: 'column',
    alignItems: 'center'
  },
  linearGradient: {
    elevation: 5,
    borderRadius: 30,
    width: width * 0.25,
    height: height * 0.055,
    marginLeft: 5,
    marginRight: 5,
  },
  image: {
    margin: 5,
    height: 60,
    width: 120,
    borderRadius: 3
  },
  cardHead: {
    margin: 5,
    borderRadius: 5
  },
  cardInner: {
    margin: -5
  },
  storeName: {
    fontSize: 20,
    marginLeft: 8,
    marginRight: 8,
    fontWeight: 'bold'
  },
  storeDiscription: {
    fontSize: 12,
    marginLeft: 8,
    marginRight: 8,
  }
});

export default DashboardScreen;
