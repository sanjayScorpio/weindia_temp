import React, { Component } from 'react';
import { StyleSheet, TouchableOpacity, View, Dimensions, Image } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import AwesomeAlert from 'react-native-awesome-alerts';
import { PrefUtils } from '../utils/PrefUtils';
import { Appbar, Text, Divider } from 'react-native-paper';
import Colors from '../styles/colors';
import FeatherIcon from 'react-native-vector-icons/Feather';
import LinearGradient from 'react-native-linear-gradient';
import { Title } from 'react-native-paper';
import user from "../../assets/images/user.jpg";
import { ScrollView } from 'react-native-gesture-handler';

const { width, height } = Dimensions.get('window');

export default class Profile extends Component {

    constructor(props) {
        super(props);
        this.state = {
            firstname: 'Your',
            lastname: 'Name',
            fullname: '',
            adress: 'Vijaynagar 1st stage Mysore',
            useremail: 'NA',
            phonenumber: 'NA',
            showAlert: false,
        };
    }


    showAlert = () => {
        this.setState({
            showAlert: true,
        });
    };

    hideAlertWithSignout = () => {
        this.setState({
            showAlert: false,
            headerMode: 'screen',
        });
        this.logoutUser();
        this.props.navigation.navigate('SignIn');
    };

    hideAlertWithoutSignout = () => {
        this.setState({
            showAlert: false,
            headerMode: 'screen',
        });
    };

    // componentDidMount() {        
    //     this._retrieveData(PrefUtils.PREF_USER_EMAIL);
    //     this._retrieveData(PrefUtils.PREF_USER_MOBILE);
    //     this._retrieveData(PrefUtils.PREF_USER_FULLNAME);
    // }

    logoutUser = () => {
        this.removeData(PrefUtils.PREF_USER_EMAIL);
        this.removeData(PrefUtils.PREF_USER_FNAME);
        this.removeData(PrefUtils.PREF_USER_FNAME);
        this.removeData(PrefUtils.PREF_USER_FULLNAME);
        this.removeData(PrefUtils.PREF_USER_ID);
        this.removeData(PrefUtils.PREF_USER_MOBILE);
    };

    // _retrieveData = async key => {
    //     try {
    //         const value = await AsyncStorage.getItem(key);
    //         console.log(key + 'sd => ' + value);
    //         if (value !== null) {
    //             // We have data!!
    //             if (key === PrefUtils.PREF_USER_EMAIL) {
    //                 this.setState({
    //                     useremail: value,
    //                 });
    //             } else if (key === PrefUtils.PREF_USER_MOBILE) {
    //                 this.setState({
    //                     phonenumber: value,
    //                 });

    //             } else if (key === PrefUtils.PREF_USER_FULLNAME) {
    //                 this.setState({
    //                     fullname: value,
    //                 });

    //             }
    //             return value;
    //         }
    //     } catch (error) {
    //         // Error retrieving data
    //         console.log(error.message);
    //     }
    // };

    renderContentData = () => (
        <View style={[styles.AlignCenter, { justifyContent: 'space-between' }]}>

            <View style={[styles.AlignCenter, { marginBottom: 20 }]}>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <FeatherIcon
                        size={18}
                        name="users"
                        style={{ backgroundColor: 'transparent' }}
                        color={Colors.primaryBlue2}
                    />
                    <Text style={styles.contentTextHeader}>Role</Text>
                </View>
                <Text style={styles.contentTextContent}>Rider</Text>
            </View>

            <View style={[styles.AlignCenter, { marginBottom: 20 }]}>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <FeatherIcon
                        size={18}
                        name="phone"
                        style={{ backgroundColor: 'transparent' }}
                        color={Colors.primaryBlue2}
                    />
                    <Text style={styles.contentTextHeader}>Phone</Text>
                </View>
                <Text style={styles.contentTextContent}>+91 9652125478</Text>
            </View>

            <View style={[styles.AlignCenter, { marginBottom: 20 }]}>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <FeatherIcon
                        size={18}
                        name="mail"
                        style={{ backgroundColor: 'transparent' }}
                        color={Colors.primaryBlue2}
                    />
                    <Text style={styles.contentTextHeader}>Email</Text>
                </View>
                <Text style={styles.contentTextContent}>ashly@gmail.com</Text>
            </View>

            <View style={[styles.AlignCenter, { marginBottom: 20 }]}>
                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                    <FeatherIcon
                        size={18}
                        name="map-pin"
                        style={{ backgroundColor: 'transparent' }}
                        color={Colors.primaryBlue2}
                    />
                    <Text style={styles.contentTextHeader}>Address</Text>
                </View>
                <Text style={[styles.contentTextContent, { flexShrink: 1 }]}>4th cross, saraswathipuram </Text>
            </View>

        </View>
    );


    renderCategories() {
        return (
          <View>
            <View style={{
              margin: 5, flexDirection: 'column',
              marginBottom: 50
            }}>
              <View style={styles.boxContent}>
                <TouchableOpacity
                  activeOpacity={0.7}
                  onPress={() => { this.props.navigation.navigate('StoreList', {}) }}>
                  <View style={styles.boxStyle}>
                    <Image source={groc} style={styles.boxImage} resizeMode='cover' />
                    {/* <View style={styles.overlay} /> */}
                    <LinearGradient colors={['rgba(255,190,32,0.4)', 'rgba(251,112,71,0.5)']} style={styles.overlayGradient} />
                    <Text style={styles.boxText} > Grocery </Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity
                  activeOpacity={0.8}
                  onPress={() => RNToasty.Info({ title: "Restaurants available Soon", titleColor: '#ffffff', withIcon: false, tintColor: '#8200e1' })}
                >
                  <View style={styles.boxStyle}>
                    <Image source={rest} style={styles.boxImage} resizeMode='cover' />
                    {/* <View style={styles.overlay} /> */}
                    <LinearGradient colors={['rgba(130,0,226,0.4)', 'rgba(0,95,199,0.5)']} style={styles.overlayGradient} />
                    <Text style={styles.boxText}> Restaurants </Text>
                  </View>
                </TouchableOpacity>
              </View>
              <View style={styles.boxContent}>
                <TouchableOpacity
                  activeOpacity={0.8}
                  onPress={() => RNToasty.Info({ title: "Make in India", titleColor: '#ffffff', withIcon: false, tintColor: '#039e6b' })}
                >
                  <View style={styles.boxStyle}>
                    <Image source={service} style={styles.boxImage} resizeMode='cover' />
                    {/* <View style={styles.overlay} /> */}
                    <LinearGradient colors={['rgba(4,159,108,0.4)', 'rgba(194,254,113,0.5)']} style={styles.overlayGradient} />
                    <Text style={styles.boxText}> Make in India </Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity
                  activeOpacity={0.8}
                  onPress={() => RNToasty.Info({ title: "Services available Soon", titleColor: '#ffffff', withIcon: false, tintColor: '#00afff' })}
                >
                  <View style={styles.boxStyle}>
                    <Image source={service} style={styles.boxImage} resizeMode='cover' />
                    {/* <View style={styles.overlay} /> */}
                    <LinearGradient colors={['rgba(0,175,255,0.2)', 'rgba(0,224,254,0.3)']} style={styles.overlayGradient} />
                    <Text style={styles.boxText}> Services </Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        )
      }
    

    renderTopContent = () => (
        <View>
            <Image source={user}
                resizeMode='cover'
                style={styles.image}
            />
            <Title style={styles.p_name}>Ashly Jackson</Title>
            <Title style={styles.p_subName}>Marketing Head, FilterQuartz</Title>
            <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                <View style={[styles.iconCircle, { backgroundColor: Colors.primaryBlue2 }]}>
                    <FeatherIcon
                        size={24}
                        name="facebook"
                        style={{ backgroundColor: 'transparent' }}
                        color={Colors.white}
                    />
                </View>
                <View style={[styles.iconCircle, { backgroundColor: Colors.red500 }]}>
                    <FeatherIcon
                        size={24}
                        name="instagram"
                        style={{ backgroundColor: 'transparent' }}
                        color={Colors.white}
                    />
                </View>
                <View style={[styles.iconCircle, { backgroundColor: Colors.primaryBlue1 }]}>
                    <FeatherIcon
                        size={24}
                        name="twitter"
                        style={{ backgroundColor: 'transparent' }}
                        color={Colors.white}
                    />
                </View>
            </View>
        </View>
    )

    removeData = async key => {
        try {
            const value = await AsyncStorage.removeItem(key);
            console.log(key + 'sd => ' + value);
            if (value !== null) {
                // We have data!!
                return value;
            }
        } catch (error) {
            // Error retrieving data
            console.log(error.message);
        }
    };

    renderAppBar() {
        return (
            <Appbar.Header>
                <Appbar.Action icon="arrow-back" onPress={() => {
                    this.props.navigation.goBack();
                }} />
                <Appbar.Content title="Account" />
            </Appbar.Header>
        );
    }

    render() {
        const { showAlert } = this.state;
        return (
            <View>
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <LinearGradient colors={[Colors.primaryBlue1, Colors.primaryBlue2]}
                        start={{ x: 0, y: 0 }} end={{ x: 0, y: 1 }}>
                        <ScrollView>
                            <View style={styles.container}>
                                <View style={styles.CardContainer}>
                                    {this.renderTopContent()}
                                    <View>
                                        <Divider />
                                        <Divider />
                                    </View>
                                    {this.renderContentData()}
                                    <View>
                                        <TouchableOpacity
                                            onPress={() => this.showAlert()}
                                            activeOpacity={0.8}>
                                            <LinearGradient colors={[Colors.primaryBlue1, Colors.primaryBlue2]}
                                                start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}
                                                style={styles.linearGradient}>
                                                <Text style={styles.text}>Logout</Text>
                                            </LinearGradient>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                        </ScrollView>
                    </LinearGradient>
                </View>


                <AwesomeAlert style={this.state.contentContainerStyle}
                    show={showAlert}
                    showProgress={false}
                    title="Log Out!"
                    message="Are sure you want to logout?"
                    closeOnTouchOutside={true}
                    closeOnHardwareBackPress={false}
                    showCancelButton={true}
                    showConfirmButton={true}
                    cancelText="No, cancel"
                    confirmText="Yes, logout!"
                    confirmButtonColor="#DD6B55"
                    onCancelPressed={() => {
                        this.hideAlertWithoutSignout();
                    }}
                    onConfirmPressed={() => {
                        this.hideAlertWithSignout();
                    }}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: width,
        height: height,
        justifyContent: 'center',
        // flexDirection : 'column'
        // alignItems : 'center'
    },
    AlignCenter: {
        alignItems: 'center'
    },
    text: {
        fontSize: 20,
        textAlign: 'center',
        padding: 10,
        color: Colors.white,
    },
    linearGradient: {
        marginTop: 10,
        marginLeft: 20,
        marginRight: 20,
        marginBottom: 10,
        elevation: 10,
        borderRadius: 30,
        // justifyContent : 'flex-end'
    },
    CardContainer: {
        width: width * 0.9,
        height: height * 0.9,
        backgroundColor: Colors.white,
        alignSelf: 'center',
        borderRadius: 10,
        justifyContent: "space-between",
        flexDirection: 'column'
    },
    image: {
        width: 124,
        height: 124,
        borderRadius: 64,
        alignSelf: 'center',
        marginTop: 10,
        marginBottom: 10,
    },
    p_name: {
        marginTop : 10,
        fontSize: 25,
        textAlign: 'center',
        color: Colors.darkGray
    },
    p_subName: {
        fontSize: 18,
        textAlign: 'center',
        color: Colors.lightGray
    },
    iconCircle: {
        width: 50,
        height: 50,
        borderRadius: 50 / 2,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 5
    },
    contentTextHeader: {
        color: Colors.lightGray,
        fontSize: 18,
        marginTop: 3,
        marginBottom: 5,
        margin: 5,
        textAlign: 'center',
        justifyContent: 'center',
        alignItems: 'center'
    },
    contentTextContent: {
        color: Colors.darkGray,
        fontSize: 20,
        margin: 3,
        textAlign: 'center',
    },
    boxStyle: {
        height: 200,
        width: 180,
        justifyContent: 'flex-end',
        alignItems: 'flex-start',
        // borderColor : Colors.primaryLight1,
        // borderWidth : 1,
        // backgroundColor : Colors.white,
        elevation: 5,
        margin: 10,
        borderRadius: 8
      },
      boxText: {
        color: Colors.white,
        fontSize: 25,
        fontWeight: '700',
        textAlign: 'center',
        marginBottom: 25,
        marginLeft: 10,
        position: 'absolute',
        // top: 0,
        // left: 0,
        bottom: 5
      },
      boxContent: {
        flexDirection: 'row',
        justifyContent: 'center'
      },
      boxImage: {
        height: 200,
        width: 180,
        borderRadius: 10
      },
      overlay: {
        ...StyleSheet.absoluteFillObject,
        backgroundColor: 'rgba(0,0,0,0.2)',
        // backgroundColor: 'rgba(255,128,102,0.3)',
        borderRadius: 10
      },
      overlayGradient: {
        ...StyleSheet.absoluteFillObject,
        // opacity : 0.6,
        // backgroundColor: 'rgba(0,0,0,0.2)',
        // backgroundColor: 'rgba(255,128,102,0.3)',
        borderRadius: 10
      },
});
