import * as React from 'react';
import { Alert, FlatList, Image, Linking, ScrollView, StyleSheet, View, Dimensions, TouchableOpacity } from 'react-native';
import Config from 'react-native-config';
import apiClient from '../controllers/ApiClient';
// import {} from 'react-navigation'
import { Appbar, Divider, List, Card, Paragraph, ThemeProvider, Portal, Text, Dialog, Button } from 'react-native-paper';
import FeatherIcon from 'react-native-vector-icons/Feather';
import LinearGradient from 'react-native-linear-gradient';
import m1 from '../../assets/images/m1.png';
import m2 from '../../assets/images/m2.png';
import m3 from '../../assets/images/m3.png';
import { Avatar, ListItem, Rating, AirbnbRating } from 'react-native-elements';
import { RNToasty } from 'react-native-toasty';
import { SliderBox } from "react-native-image-slider-box";
import Colors from '../styles/colors';

const { width, height } = Dimensions.get('window')

class CardDetails extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            storeList: [],
            dialogVisible: false,
            feedbackVisible: false,
            feedbackText: '',
            modalVisible: false,
            savedAddressList: [],
            results_found: 0,
            locationPermission: '',
            userData: '',
            formatted_address: '',
            short_address: '',
            refresh: false,
            isFetching: true,
            SliderImages: [
                // "https://www.iamexpat.nl/sites/default/files/styles/article--full/public/plant-based-diet-budget-friendlt.jpg?itok=MT8jdzia",
                "https://miro.medium.com/max/1024/1*YLlZ96J3p8GFkIh1USVMzg.jpeg",
                "https://www.triumph30.org/wp-content/uploads/2019/10/The-studious-believer.jpg",
                "https://i0.wp.com/geologyconcepts.com/wp-content/uploads/2019/09/UPSC-Civil-Services-Prelims-Result-2019-1280x720.jpg?resize=1024%2C576&ssl=1",
                "https://leverageedu.com/blog/wp-content/uploads/2019/09/Method-Study.png",
                "https://cdn-image.hipwee.com/wp-content/uploads/2018/01/hipwee-cara-belajar-efektif-750x400.jpg",
            ]
        };
        // console.log("userData",this.userData);
    }


    setDialogVisible = () =>
        this.setState({ dialogVisible: !this.state.dialogVisible });

    setFeedbackText = (text) => {
        this.setState({ feedbackText: text })
    }

    // Check the status of a single permission
    componentDidMount() {
        // this.getCustomerAlternateAddress();
    }


    renderAppBar() {
        return (
            <Appbar.Header style={{ backgroundColor: Colors.white }}>
                <Appbar.Action icon={() => <FeatherIcon
                    size={24}
                    name="menu"
                    style={{ backgroundColor: 'transparent' }}
                    color={Colors.primaryGreen2}
                />}
                    // onPress={() => { this.props.navigation.dispatch(DrawerActions.toggleDrawer())}} 
                    onPress={() => this.props.navigation.openDrawer()}
                />
                <Appbar.Content title="We India" color={Colors.darkGray} />
                <Appbar.Action icon={() => <FeatherIcon
                    size={24}
                    name="search"
                    style={{ backgroundColor: 'transparent' }}
                    color={Colors.primaryGreen2}
                />} onPress={() => { this.props.navigation.goBack() }} />
                <Appbar.Action icon={() => <FeatherIcon
                    size={24}
                    name="more-vertical"
                    style={{ backgroundColor: 'transparent' }}
                    color={Colors.primaryGreen2}
                />} onPress={() => { this.props.navigation.goBack() }} />
            </Appbar.Header>
        );
    }

    renderCategories() {
        return (
            <View>
                <View style={{margin: 5, flexDirection: 'column'}}>
                    <TouchableOpacity
                        activeOpacity={0.8}
                        onPress={this.setDialogVisible}
                    >
                        <View style={styles.boxStyle}>
                            <Image source={m1} style={styles.boxImage} resizeMode='cover' />
                            <LinearGradient colors={['rgba(255,190,32,0.8)', 'rgba(251,112,71,0.8)']}
                                start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}
                                style={styles.overlay}>
                                <View style={styles.innerBoxStyle}>
                                    <Text style={styles.boxText}> We India </Text>
                                    <Text> We India </Text>
                                </View>
                            </LinearGradient>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                        activeOpacity={0.7}
                    // onPress={() => { this.props.navigation.navigate('StoreList', {}) }}
                    >
                        <View style={styles.boxStyle}>
                            <Image source={m2} style={styles.boxImage} resizeMode='cover' />
                            <LinearGradient colors={['rgba(4,159,108,0.3)', 'rgba(194,254,113,0.5)']}
                                start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}
                                style={styles.overlay}>
                                <Text style={styles.boxText}> We India </Text>
                            </LinearGradient>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                        activeOpacity={0.7}
                    // onPress={() => { this.props.navigation.navigate('StoreList', {}) }}
                    >
                        <View style={styles.boxStyle}>
                            <Image source={m3} style={styles.boxImage} resizeMode='cover' />
                            <LinearGradient colors={['rgba(130,0,226,0.4)', 'rgba(0,95,199,0.5)']}
                                start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}
                                style={styles.overlay}>
                                <Text style={styles.boxText}> We India </Text>
                            </LinearGradient>
                        </View>
                    </TouchableOpacity>
                    {/*                     
                    <View style={styles.boxStyle}>
                        <Image
                            source={m3}
                            style={styles.boxImage}
                            resizeMode='cover'
                        />
                        <LinearGradient colors={[Colors.easyButtonStart, Colors.easyButtonEnd]}
                            start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}
                            style={styles.linearGradient}>
                            <Text style={styles.boxText}> We India </Text>
                             <View style={styles.overlay} />

                        </LinearGradient>
                    </View> */}
                    {/* </View> */}
                </View>
            </View>
        )
    }

    renderSlider() {
        return (
            <Card style={{ marginTop: 1 }}>
                <SliderBox
                    images={this.state.SliderImages}
                    sliderBoxHeight={200}
                    onCurrentImagePressed={index => RNToasty.Show({ title: `We india offer ${index + 1}` })}
                    dotColor={Colors.white}
                    borderRadius={5}
                    inactiveDotColor="#90A4AE"
                    // paginationBoxVerticalPadding={20}
                    autoplay
                    circleLoop
                    resizeMethod={'resize'}
                />
                {/* <View style = {styles.overlay}></View> */}
            </Card>
        )
    }

    onPressRatings = () => {
        return (
            <Portal>
                <Dialog
                    visible={this.state.dialogVisible}
                    onDismiss={this.setDialogVisible}>
                    <Dialog.Title style={styles.centerTitle}>
                        How was your experience with us?
              </Dialog.Title>
                    <Dialog.Content>
                        <View>
                            <AirbnbRating
                                color={Colors.primary}
                                count={5}
                                reviews={['🤮 Terrible  ', '😐 OK  ', '😊 Good  ', '😀 wow  ', '😍 Amazing  ']}
                                defaultRating={4}
                                size={30}
                            />
                        </View>
                    </Dialog.Content>
                    <Dialog.Actions>
                        <Button
                            style={styles.m_10}
                            mode="text"
                            onPress={this.setDialogVisible}>
                            Done
                </Button>
                        <Button
                            style={styles.m_10}
                            mode="text"
                            onPress={this.setDialogVisible}>
                            Cancel
                </Button>
                    </Dialog.Actions>
                </Dialog>
            </Portal>
        );
    };


    renderStatusComponent() {
        return (
            <View style={{ justifyContent: 'space-between', backgroundColor: Colors.white, borderRadius: 10, borderColor: Colors.primaryGreen1, borderWidth: 2, width: width * 0.95, height: height * 0.2, elevation: 5, paddingBottom: 10 }}>
                <View style={{ flexDirection: 'row', alignItems: 'center', alignContent: 'center', paddingTop: 10 }}>
                    <Text style={{ fontSize: 22, color: Colors.darkGray, paddingLeft: 15, paddingRight: 10 }}>Status</Text>
                    <FeatherIcon name='info' size={20} style={{ backgroundColor: 'transparent' }} color={Colors.darkGray} />
                </View>

                <View style={{ flexDirection: 'row', justifyContent: 'space-evenly' }}>
                    <View>
                        <Text style={{ fontSize: 16, color: Colors.primaryGreen1, paddingLeft: 8, paddingTop: 5 }}>Today's status</Text>

                        <View style={{ margin: 5, justifyContent: 'space-around', backgroundColor: Colors.white, borderRadius: 5, borderColor: Colors.primaryGreen1, borderWidth: 1, width: width * 0.42, height: height * 0.08 }}>
                            <View style={{ flexDirection: 'row', padding: 2, justifyContent: 'space-between' }}>
                                <Text style={{ fontSize: 12, color: Colors.primaryGreen1, padding: 5 }}>Card's Studied</Text>
                                <Text style={{ fontSize: 12, color: Colors.darkGray, padding: 5 }}>21</Text>
                            </View>
                            <View style={{ flexDirection: 'row', padding: 2, justifyContent: 'space-between' }}>
                                <Text style={{ fontSize: 12, color: Colors.primaryGreen1, padding: 5 }}>Total time spent</Text>
                                <Text style={{ fontSize: 12, color: Colors.darkGray, padding: 5 }}>2153 m</Text>
                            </View>
                        </View>
                    </View>
                    <View>
                        <Text style={{ fontSize: 16, color: Colors.primaryGreen1, paddingLeft: 8, paddingTop: 5 }}>Overall status</Text>

                        <View style={{ margin: 5, justifyContent: 'space-around', backgroundColor: Colors.white, borderRadius: 5, borderColor: Colors.primaryGreen1, borderWidth: 1, width: width * 0.42, height: height * 0.08 }}>
                            <View style={{ flexDirection: 'row', padding: 2, justifyContent: 'space-between' }}>
                                <Text style={{ fontSize: 12, color: Colors.primaryGreen1, padding: 5 }}>Card's due</Text>
                                <Text style={{ fontSize: 12, color: Colors.darkGray, padding: 5 }}>215</Text>
                            </View>
                            <View style={{ flexDirection: 'row', padding: 2, justifyContent: 'space-between' }}>
                                <Text style={{ fontSize: 12, color: Colors.primaryGreen1, padding: 5 }}>Time left</Text>
                                <Text style={{ fontSize: 12, color: Colors.darkGray, padding: 5 }}>25613 m</Text>
                            </View>
                        </View>
                    </View>
                </View>
                <View>

                </View>
            </View>
        )
    }

    render() {
        return (
            <View>
                {this.renderAppBar()}
                {/* {this.renderCategories1()} */}
                <View style={{ height: height * 0.92, paddingBottom: 10 }}>
                    <ScrollView>
                        {this.renderSlider()}
                        <View style={styles.container}>
                            {this.renderCategories()}
                            {this.renderStatusComponent()}
                            {this.onPressRatings()}
                        </View>
                    </ScrollView>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        // flex: 1,
        justifyContent: 'center',
        flexDirection: 'column',
        alignItems: 'center'
    },
    linearGradient: {
        elevation: 5,
        position: 'absolute',
        borderRadius: 5,
        width: width * 0.95,
        height: height * 0.22,
        opacity: 0.7
        // marginLeft: 5,
        // marginRight: 5,
    },
    image: {
        margin: 5,
        height: 60,
        width: 120,
        borderRadius: 3
    },
    cardHead: {
        margin: 5,
        borderRadius: 5
    },
    cardInner: {
        margin: -5
    },
    storeName: {
        fontSize: 20,
        marginLeft: 8,
        marginRight: 8,
        fontWeight: 'bold'
    },
    storeDiscription: {
        fontSize: 12,
        marginLeft: 8,
        marginRight: 8,
    },
    boxStyle: {
        height: height * 0.22,
        width: width * 0.95,
        justifyContent: 'flex-end',
        alignItems: 'flex-start',
        // borderColor : Colors.primaryLight1,
        // borderWidth : 1,
        // backgroundColor : Colors.white,
        elevation: 5,
        margin: 10,
        borderRadius: 8
    },
    innerBoxStyle: {
        height: height * 0.22,
        width: width * 0.95,

    },
    boxText: {
        color: Colors.white,
        fontSize: 32,
        // fontWeight: '700',
        textAlign: 'center',
        marginBottom: 25,
        marginLeft: 10,
        position: 'absolute',
        // top: 0,
        // left: 0,
        bottom: 5
    },
    boxContent: {
        flexDirection: 'row',
        justifyContent: 'center'
    },
    boxImage: {
        width: width * 0.95,
        height: height * 0.22,
        borderRadius: 10
    },
    overlay: {
        ...StyleSheet.absoluteFillObject,
        // backgroundColor: 'rgba(0,0,0,0.3)',
        borderRadius: 10
    },
    m_10: {
        margin: 10
    },
});

export default CardDetails;
