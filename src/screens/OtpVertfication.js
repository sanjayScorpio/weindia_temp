/**
 * @flow
 * @format
 */

import * as React from 'react';
import { Dimensions, StyleSheet, View, TouchableOpacity, Image, KeyboardAvoidingView ,ImageBackground} from 'react-native';
import { Text, TextInput, HelperText } from 'react-native-paper';
import { Input } from 'react-native-elements';
import SmoothPinCodeInput from 'react-native-smooth-pincode-input';
import Logo from '../components/Logo';
import Colors from '../styles/colors';
import LinearGradient from 'react-native-linear-gradient';
import bg from '../../assets/images/bgm2.png';
import FeatherIcon from 'react-native-vector-icons/Feather';

const { width, height } = Dimensions.get('window');

class OtpVertfication extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            phone: 0,
            phoneError: false,
            code: ''
        };
        // this.onInputChange = this.onInputChange.bind(this);
        // this.handleOTPVerification = this.handleOTPVerification.bind(this);
    }

    handleOTPVerification = () => {
        this.props.navigation.navigate('App')
    }

    render() {
        return (
            <ImageBackground source={bg} style={{ width: '100%', height: '80%' }}>
                <View style={styles.container}>
                    <View style={styles.logoContainer}>
                        <Logo />
                    </View>

                    <View style={styles.bodyContainer}>

                        <Text style={styles.welcomeText}>OTP Verification</Text>
                        <Text style={styles.subText}>Enter the 4 digit OTP code you received</Text>

                        <View style={{ margin: 20, alignItems: 'center' }}>
                    <SmoothPinCodeInput
                        //   ref={this.pinInput}
                        value={this.state.code}
                        codeLength={4}
                        onTextChange={code => this.setState({ code })}
                        // onFulfill={this._checkCode}
                        onBackspace={() => console.log('No more back.')}
                        // placeholder="⭑"
                        cellSpacing = {35}
                        cellStyle={{
                            borderWidth: 1.5,
                            borderRadius: 8,
                            margin : 1,
                            borderColor: Colors.primaryGreen1,
                            backgroundColor: 'white',
                            elevation : 2,

                        }}
                        cellStyleFocused={{
                            borderColor: Colors.green500,
                            backgroundColor: Colors.bluish,
                            borderWidth: 3.5,
                        }}
                        textStyle={{
                            fontSize: 24,
                            color: Colors.darkGray
                        }}
                        textStyleFocused={{
                            color: 'crimson'
                        }}
                    />
                </View>


                        <TouchableOpacity
                            onPress={this.handleOTPVerification}
                            activeOpacity={0.8}>
                            <LinearGradient colors={[Colors.primaryGreen1, Colors.primaryGreen2]}
                                start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}
                                style={styles.linearGradient}>
                                <Text style={styles.text}>Verify & Continue</Text>
                            </LinearGradient>
                        </TouchableOpacity>

                        <TouchableOpacity
                            // onPress={this.handleOTPVerification}
                            activeOpacity={0.8}>
                            <LinearGradient colors={[Colors.primaryGreen1, Colors.primaryGreen2]}
                                start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}
                                style={styles.linearGradient}>
                                <Text style={styles.text}>Resend</Text>
                            </LinearGradient>
                        </TouchableOpacity>

                        <Text style={styles.linkText}>
                            www.weindia.com
                        </Text>

                    </View>
                </View>
            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'space-between',
        margin: 10,
        alignItems: 'center'
        // alignSelf : 'center'
    },
    text: {
        fontSize: 20,
        textAlign: 'center',
        padding: 14,
        color: Colors.white,
    },
    linearGradient: {
        marginTop: 10,
        marginLeft: 20,
        marginRight: 20,
        marginBottom: 10,
        elevation: 10,
        borderRadius: 30
    },
    inputContainerStyle: {
        padding: 3,
        backgroundColor: 'white',
        borderRadius: 50,
        borderTopLeftRadius: 50,
        borderWidth: 1.5,
        borderTopRightRadius: 50,
        borderColor: Colors.primaryGreen1
    },
    logoContainer: {
        marginBottom: 30,
        marginTop: 30,
        height: 200,
        width: 200,
        backgroundColor: Colors.white,
        justifyContent: 'center',
        borderColor: Colors.primaryGreen1,
        borderWidth: 0.8,
        borderRadius: 100
    },
    bodyContainer: {
        width: width * 0.94,
        backgroundColor: Colors.white,
        elevation: 10,
        height: height * 0.6,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
        justifyContent : 'center',
    },
    welcomeText: {
        alignSelf: 'center',
        margin: 12,
        fontSize: 28,
        color: Colors.primaryGreen1,
        textDecorationLine: 'underline'
    },
    subText: {
        alignSelf: 'center',
        margin: 20,
        fontSize: 18,
        textAlign: 'center',
        color: Colors.darkGray
    },
    linkText: {
        alignSelf: 'center',
        margin: 20,
        fontSize: 20,
        color: Colors.primaryGreen1,
        textAlign: 'center',
        textDecorationLine: 'underline'
    },
});
export default OtpVertfication;


{/* <Text style={{ alignSelf: 'center', marginBottom: 20, fontSize: 35, color: Colors.primaryBlue1 }}>Verfiy OTP</Text> */}

                {/* <View style={{ margin: 20, alignItems: 'center' }}>
                    <SmoothPinCodeInput
                        //   ref={this.pinInput}
                        value={this.state.code}
                        codeLength={4}
                        onTextChange={code => this.setState({ code })}
                        // onFulfill={this._checkCode}
                        onBackspace={() => console.log('No more back.')}
                        // placeholder="⭑"
                        cellSpacing = {30}
                        cellStyle={{
                            borderWidth: 1.5,
                            borderRadius: 8,
                            margin : 1,
                            borderColor: Colors.primaryGreen1,
                            backgroundColor: 'white',
                            elevation : 2,

                        }}
                        cellStyleFocused={{
                            borderColor: Colors.green500,
                            backgroundColor: Colors.bluish,
                            borderWidth: 3.5,
                        }}
                        textStyle={{
                            fontSize: 24,
                            color: Colors.darkGray
                        }}
                        textStyleFocused={{
                            color: 'crimson'
                        }}
                    />
                </View>

                <TouchableOpacity
                    onPress={this.handleOTPVerification}
                    activeOpacity={0.8}>
                    <LinearGradient colors={[Colors.primaryGreen1, Colors.primaryGreen2]}
                        start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}
                        style={styles.linearGradient}>
                        <Text style={styles.text}>Verify & Continue</Text>
                    </LinearGradient>
                </TouchableOpacity> */}