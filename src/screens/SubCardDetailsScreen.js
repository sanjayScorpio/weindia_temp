/* eslint-disable react/jsx-filename-extension */
/**
 * @flow
 * @format
 */
import * as React from 'react';
import { TouchableOpacity, FlatList, Image, StyleSheet, TextInput, View, Dimensions } from 'react-native';
import {
  Button,
  Card,
  Divider,
  IconButton,
  List,
  Portal,
  Appbar,
  Text,
  Title,
} from 'react-native-paper';
import Icon from 'react-native-vector-icons/Entypo';
import { PrefUtils } from '../utils/PrefUtils';
import AsyncStorage from '@react-native-community/async-storage';
import { RNToasty } from 'react-native-toasty';
import LinearGradient from 'react-native-linear-gradient';
import Colors from '../styles/colors';
import { ScrollView } from 'react-native-gesture-handler';
import agri from '../../assets/images/agriculture.jpg'

const { width, height } = Dimensions.get('window');

class SubCardDetailsScreen extends React.Component {

  constructor(props) {
    super(props);
    this.navigation = props.navigation;
    // this.state.cartItemsData = props.navigation.getParam('cartItems', {});
    // console.log("Cart Data:", JSON.stringify(this.state.cartItemsData))
    // this.state.storeListData = props.navigation.getParam('storeListData', {});
    // console.log("Storelist Data:", JSON.stringify(this.state.storeListData))

    this.state = {
      attendanceStatus: 'Punched In'
    };
    this.handleAttendanceDetails = this.handleAttendanceDetails.bind(this)
  }

  // componentDidMount() {
  //   // retrieveData(PrefUtils.PREF_CART_ITEMS)
  //   //   .then(cartItemsData =>
  //   //     this.setState({
  //   //       cartItemsData: JSON.parse(cartItemsData),
  //   //       newCartItemList: cartItemsData.ordered_items,
  //   //     }),
  //   //   )
  //   //   .catch(error => console.error(error));
  //   // retrieveData(PrefUtils.PREF_STORE_LIST_DATA)
  //   //   .then(storeListData =>
  //   //     this.setState({storeListData: JSON.parse(storeListData)}),
  //   //   )
  //   //   .catch(error => console.error(error));
  //   let keys = [
  //     PrefUtils.PREF_CART_ITEMS,
  //     PrefUtils.PREF_STORE_LIST_DATA,
  //     PrefUtils.PREF_USER_DATA,
  //   ];
  //   AsyncStorage.multiGet(keys, (err, stores) => {
  //     stores.map((result, i, store) => {
  //       // get at each store's key/value so you can work with it
  //       let key = store[i][0];
  //       let value = JSON.parse(store[i][1]);

  //       if (key === PrefUtils.PREF_CART_ITEMS) {
  //         if (value === null || value === undefined) {
  //           value = { ordered_items: [] };
  //         }
  //         this.setState({
  //           cartItemsData: value,
  //           newCartItemList: value.ordered_items,
  //         });
  //       } else if (key === PrefUtils.PREF_STORE_LIST_DATA) {
  //         this.setState({ storeListData: value });
  //       } else if (key === PrefUtils.PREF_USER_DATA) {
  //         const { id } = value;
  //         this.setState({ cid: id });
  //         this.getSavedAddress(id);
  //       }
  //     });
  //   });

  //   console.log('====================================');
  //   console.log('Store Selected', this.state.storeListData);
  //   console.log('====================================');

  //   console.log('====================================');
  //   console.log('Store items in cart', this.state.cartItemsData);
  //   console.log('====================================');
  // }
  handleAttendanceDetails = () => {
    this.props.navigation.navigate('AddAddress');
  }

  renderAppBar() {
    return (
      <Appbar.Header style={{ backgroundColor: Colors.white }}>
        <Appbar.Action icon="arrow-back" color={Colors.primaryGreen1} onPress={() => { this.props.navigation.goBack() }} />
        <Appbar.Content title="Indian Economy" color={Colors.darkGray} />
      </Appbar.Header>
    );
  }

  renderTopFixedCard() {
    return (
      <View style={styles.fixedCardContainer}>
        <View style={{ flexDirection: 'row', justifyContent: 'space-evenly' }}>
          <View style={styles.cardFlex}>
            <View style={styles.innerRow}>
              <Text style={styles.mainText}>Card Studied</Text>
              <Text style={{ fontSize: 12 }}>231</Text>
            </View>
            <View style={styles.innerRow}>
              <Text style={styles.mainText}>Total hours spent</Text>
              <Text style={{ fontSize: 12 }}>{Math.floor(21523 / 60)} H</Text>
            </View>
          </View>
          <View style={{ width: 0.5, height: height * 0.070, backgroundColor: Colors.darkGray }} />
          <View style={styles.cardFlex}>
            <View style={styles.innerRow}>
              <Text style={styles.mainText}>Card due</Text>
              <Text style={{ fontSize: 12 }}>225</Text>
            </View>
            <View style={styles.innerRow}>
              <Text style={styles.mainText}>Time left</Text>
              <Text style={{ fontSize: 12 }}>{Math.floor(21523 / 60)} H</Text>
            </View>
          </View>
          <View style={{ flexDirection: 'column', justifyContent: 'center' }}>
            <Icon size={25} name="reply" style={{ backgroundColor: 'transparent' }} color={Colors.amber500} />
          </View>
          <View style={{ flexDirection: 'column', justifyContent: 'center' }}>
            <Icon size={25} name="star" style={{ backgroundColor: 'transparent' }} color={Colors.amber500} />
          </View>
        </View>
      </View>
    )
  }

  renderQuestionBox() {
    return (
      <View style={styles.questionCard}>
        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
          <Text style={{ color: Colors.darkGray, fontSize: 12 }}>
            Card : 22 / 57
          </Text>
          <Text style={{ color: Colors.darkGray, fontSize: 12 }}>
            Time : 07 : 36
          </Text>
        </View>
        <View style={{ justifyContent: 'center',height: height * 0.06 }}>
          <ScrollView>
          <Text style={{ color: Colors.darkGray, fontSize: 22, textAlign: 'center' }}>
            Agriculture role in Indian Economy
          </Text>
          </ScrollView>
        </View>
      </View>
    )
  }

  renderCardContent() {
    return (
      <View style={styles.baseCard}>
        <View style={{ flex: 0.83, paddingTop: 8 }}>
          <ScrollView >
            <View style={{ margin: 10, marginBottom: 80 }}>
              <Image
                source={agri}
                style={{
                  width: width * 0.9,
                  height: height * 0.3,
                  borderRadius: 10,
                  margin: 10
                }}
                resizeMode='cover'
              />
              <Text style={{ textAlign: 'center' }}>
                ಇಂಗ್ಲಿಷ್‌‌ ಮೂಲಕ ಅನುವಾದಿತ-ಭಾರತ ಸರ್ಕಾರದ ಒಂದು ಶಾಖೆಯಾದ ಕೃಷಿ ಮತ್ತು ರೈತರ ಕಲ್ಯಾಣ ಸಚಿವಾಲಯವು
                ಭಾರತದಲ್ಲಿ ಕೃಷಿಗೆ ಸಂಬಂಧಿಸಿದ ನಿಯಮಗಳು ಮತ್ತು ನಿಯಮಗಳು ಮತ್ತು ಕಾನೂನುಗಳನ್ನು ರೂಪಿಸುವ ಮತ್ತು ನಿರ್ವಹಿಸುವ
                ಉನ್ನತ ಸಂಸ್ಥೆಯಾಗಿದೆ. ಸಚಿವಾಲಯದ ವ್ಯಾಪ್ತಿಯ ಮೂರು ವಿಶಾಲ ಕ್ಷೇತ್ರಗಳು ಕೃಷಿ, ಆಹಾರ ಸಂಸ್ಕರಣೆ ಮತ್ತು ಸಹಕಾರ.
                ಕೃಷಿ ಸಚಿವಾಲಯದ ಕೃಷಿ ಸಚಿವ ನರೇಂದ್ರ ಸಿಂಗ್ ತೋಮರ್ ನೇತೃತ್ವ ವಹಿಸಿದ್ದಾರೆ
         </Text>
              <Text style={{ textAlign: 'center', marginTop: 10 }}>
                The history of Agriculture in India dates back to Indus Valley Civilization and even before
                that in some places of Southern India.[1] India ranks second worldwide in farm outputs.
                As per 2018, agriculture employed 50% of the Indian work force and contributed 17–18% to country's GDP.
         </Text>
              <Text style={{ textAlign: 'center' }}>
                ಇಂಗ್ಲಿಷ್‌‌ ಮೂಲಕ ಅನುವಾದಿತ-ಭಾರತ ಸರ್ಕಾರದ ಒಂದು ಶಾಖೆಯಾದ ಕೃಷಿ ಮತ್ತು ರೈತರ ಕಲ್ಯಾಣ ಸಚಿವಾಲಯವು
                ಭಾರತದಲ್ಲಿ ಕೃಷಿಗೆ ಸಂಬಂಧಿಸಿದ ನಿಯಮಗಳು ಮತ್ತು ನಿಯಮಗಳು ಮತ್ತು ಕಾನೂನುಗಳನ್ನು ರೂಪಿಸುವ ಮತ್ತು ನಿರ್ವಹಿಸುವ
                ಉನ್ನತ ಸಂಸ್ಥೆಯಾಗಿದೆ. ಸಚಿವಾಲಯದ ವ್ಯಾಪ್ತಿಯ ಮೂರು ವಿಶಾಲ ಕ್ಷೇತ್ರಗಳು ಕೃಷಿ, ಆಹಾರ ಸಂಸ್ಕರಣೆ ಮತ್ತು ಸಹಕಾರ.
                ಕೃಷಿ ಸಚಿವಾಲಯದ ಕೃಷಿ ಸಚಿವ ನರೇಂದ್ರ ಸಿಂಗ್ ತೋಮರ್ ನೇತೃತ್ವ ವಹಿಸಿದ್ದಾರೆ
         </Text>
              <Text style={{ textAlign: 'center', marginTop: 10 }}>
                The history of Agriculture in India dates back to Indus Valley Civilization and even before
                that in some places of Southern India.[1] India ranks second worldwide in farm outputs.
                As per 2018, agriculture employed 50% of the Indian work force and contributed 17–18% to country's GDP.
         </Text>
            </View>
          </ScrollView>
        </View>
        <View style={{ flex: 0.16,justifyContent : 'flex-start',marginTop : 10}}>
          <View style={{flexDirection : 'row', justifyContent : 'space-between', alignItems : 'center', alignSelf : 'center'}}>
          <TouchableOpacity
            activeOpacity={0.8}>
            <LinearGradient colors={[Colors.hardButtonStart, Colors.hardButtonEnd]}
              start={{ x: 0, y: 1 }} end={{ x: 1, y: 0 }}
              style={styles.linearGradient}>
                <View style={{ justifyContent : 'center', alignItems : 'center', alignSelf : 'center'}}>
              <Text style={styles.text}>Hard</Text>
              <Text style={styles.text2}>00 Hr, 5 min</Text></View>
            </LinearGradient>
          </TouchableOpacity>

          <TouchableOpacity
            activeOpacity={0.8}>
            <LinearGradient colors={[Colors.mediumButtonStart, Colors.mediumButtonEnd]}
              start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}
              style={styles.linearGradient}>
                <View style={{ justifyContent : 'center', alignItems : 'center', alignSelf : 'center'}}>
              <Text style={styles.text}>Medium</Text>
              <Text style={styles.text2}>00 Hr, 5 min</Text></View>
            </LinearGradient>
          </TouchableOpacity>
          
          <TouchableOpacity
            activeOpacity={0.8}>
            <LinearGradient colors={[Colors.easyButtonStart, Colors.easyButtonEnd]}
              start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}
              style={styles.linearGradient}>
                <View style={{ justifyContent : 'center', alignItems : 'center', alignSelf : 'center'}}>
              <Text style={styles.text}>Easy</Text>
              <Text style={styles.text2}>00 Hr, 5 min</Text></View>
            </LinearGradient>
          </TouchableOpacity>
        </View>
        </View>
      </View>
    );
  }



  render() {
    return (
      <View >
        {this.renderAppBar()}
        <View style={styles.container}>
          <View>
            {this.renderTopFixedCard()}
          </View>
          {this.renderQuestionBox()}
          <View>
            {this.renderCardContent()}
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({

  container: {
    // flex: 1,
    justifyContent: 'space-between',
    flexDirection: 'column',
    alignItems: 'center'
  },
  linearGradient: {
    elevation: 5,
    borderRadius: 30,
    width : width * 0.25,
    height : height * 0.055,
    marginLeft : 5,
    marginRight : 5,
  },
  text: {
    fontSize: 16,
    textAlign: 'center',
    paddingTop: 5,
    color: Colors.white,
    marginBottom : -2
},
text2: {
  fontSize: 10,
  textAlign: 'center',
  color: Colors.white,
},

  //  top fixed Card
  fixedCardContainer: {
    justifyContent: 'center',
    backgroundColor: Colors.white,
    width: width,
    height: height * 0.1,
    borderColor: Colors.primaryGreen2,
    borderWidth: 1,
    borderRadius: 5,
    marginTop: 3,
  },
  innerRow: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  cardFlex: {
    flexDirection: 'column',
    flex: 0.4,
    justifyContent: 'space-around'
  },
  cardDivider: {

  },
  mainText: {
    color: Colors.primaryGreen2,
    fontSize: 12
  },
  subText: {},

  // render Card content
  baseCard: {
    alignItems: 'center',
    justifyContent: 'center',
    width: width,
    height: height * 0.75,
    backgroundColor: Colors.white,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    elevation: 20,
  },

  // Question Card
  questionCard: {
    height: height * 0.1,
    width: width * 0.9,
    // backgroundColor : Colors.white,
    flexDirection: 'column',
    justifyContent: 'space-around'
  }


}
);

export default SubCardDetailsScreen;