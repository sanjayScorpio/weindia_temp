/**
 * @flow
 * @format
 */

import * as React from 'react';
import { Dimensions, StyleSheet, View, TouchableOpacity, Image, KeyboardAvoidingView, ImageBackground } from 'react-native';
import { Text, TextInput, HelperText } from 'react-native-paper';
import { Input } from 'react-native-elements';
import Logo from '../components/Logo';
import Colors from '../styles/colors';
import LinearGradient from 'react-native-linear-gradient';
import bg from '../../assets/images/bgm2.png';
import FeatherIcon from 'react-native-vector-icons/Feather';



const { width, height } = Dimensions.get('window');
const phoneRegex = /^(?:(?:\+|0{0,2})91(\s*[\ -]\s*)?|[0]?)?[789]\d{9}|(\d[ -]?){9}\d$/;
// const phoneRegex = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/

class MobileSignIn extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            phone: 0,
            phoneError: false
        };
        // this.onInputChange = this.onInputChange.bind(this);
        // this.handleSignIn = this.handleSignIn.bind(this);
    }

    _isPhoneValid = () => {
        return (
            phoneRegex.test(this.state.phone)
        );
    };

    handleSignIn = () => {
        // if (this._isPhoneValid()) {
        this.props.navigation.navigate('OtpVertfication')
        // } else {
        // this.setState({ phoneError: true })
        // return
        // }
    }

    render() {
        return (
            <ImageBackground source={bg} style={{ width: '100%', height: '80%' }}>
                <View style={styles.container}>
                    <View style={styles.logoContainer}>
                        <Logo />
                    </View>

                    <View style={styles.bodyContainer}>
                        <View>
                        <Text style={styles.welcomeText}>Welcome</Text>
                        <Text style={styles.subText}>Enter your mobile number</Text>
                        </View>

                        <View>
                        <View style={{ margin: 20 }}>
                            <Input
                                placeholder='Phone Number'
                                leftIcon={<FeatherIcon
                                    size={24}
                                    name="user"
                                    style={{ backgroundColor: 'transparent', marginRight: 20 }}
                                    color={Colors.primaryGreen1}
                                />}
                                value={this.state.phone}
                                onChangeText={phone => this.setState({ phone })}
                                underlineColorAndroid='transparent'
                                keyboardType='number-pad'
                                inputContainerStyle={{ marginLeft: -10, borderBottomWidth: 0 }}
                                containerStyle={styles.inputContainerStyle}
                            />
                        </View>

                        <TouchableOpacity
                            onPress={this.handleSignIn}
                            activeOpacity={0.8}>
                            <LinearGradient colors={[Colors.primaryGreen1, Colors.primaryGreen2]}
                                start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}
                                style={styles.linearGradient}>
                                <Text style={styles.text}>Get OPT</Text>
                            </LinearGradient>
                        </TouchableOpacity>
                        </View>

                        <View>
                        <Text style={styles.descText}>
                            By clicking get otp button you agree to our Terms and Conditions and Privacy Policy
                        </Text>

                        <Text style={styles.linkText}>
                            www.weindia.com
                        </Text>
                        </View>


                    </View>
                </View>
            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'space-between',
        margin: 10,
        alignItems: 'center'
        // alignSelf : 'center'
    },
    text: {
        fontSize: 20,
        textAlign: 'center',
        padding: 14,
        color: Colors.white,
    },
    linearGradient: {
        marginTop: 10,
        marginLeft: 20,
        marginRight: 20,
        marginBottom: 10,
        elevation: 10,
        borderRadius: 30
    },
    inputContainerStyle: {
        padding: 3,
        backgroundColor: 'white',
        borderRadius: 50,
        borderTopLeftRadius: 50,
        borderWidth: 1.5,
        borderTopRightRadius: 50,
        borderColor: Colors.primaryGreen1
    },
    logoContainer: {
        // flex : 0.3,
        marginBottom: 30,
        marginTop: 30,
        height: 200,
        width: 200,
        backgroundColor: Colors.white,
        justifyContent: 'center',
        borderColor: Colors.primaryGreen1,
        borderWidth: 0.8,
        borderRadius: 100
    },
    bodyContainer: {
        width: width * 0.94,
        backgroundColor: Colors.white,
        elevation: 10,
        height: height * 0.6,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
        justifyContent : 'center',
    },
    welcomeText: {
        alignSelf: 'center',
        margin: 12,
        fontSize: 28,
        textDecorationLine: 'underline',
        color: Colors.primaryGreen1
    },
    subText: {
        alignSelf: 'center',
        margin: 20,
        fontSize: 20,
        color: Colors.darkGray
    },
    descText: {
        alignSelf: 'center',
        margin: 18,
        fontSize: 16,
        color: Colors.darkGray,
        textAlign: 'center'
    },
    linkText: {
        alignSelf: 'center',
        margin: 10,
        fontSize: 20,
        color: Colors.primaryGreen1,
        textAlign: 'center',
        textDecorationLine: 'underline'
    },
});
export default MobileSignIn;
