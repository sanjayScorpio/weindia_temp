import React, { Component } from 'react';
import { StyleSheet, TouchableOpacity, View, Dimensions, Image } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import AwesomeAlert from 'react-native-awesome-alerts';
import { PrefUtils } from '../utils/PrefUtils';
import { Appbar, Text, Divider } from 'react-native-paper';
import Colors from '../styles/colors';
import FeatherIcon from 'react-native-vector-icons/Feather';
import LinearGradient from 'react-native-linear-gradient';
import { Title } from 'react-native-paper';
import user from "../../assets/images/user.jpg";
import { ScrollView } from 'react-native-gesture-handler';

const { width, height } = Dimensions.get('window');

export default class Profile extends Component {

    constructor(props) {
        super(props);
        this.state = {
            firstname: 'Your',
            lastname: 'Name',
            fullname: '',
            adress: 'Vijaynagar 1st stage Mysore',
            useremail: 'NA',
            phonenumber: 'NA',
            showAlert: false,
        };
    }

    renderAppBar() {
        return (
            <Appbar.Header style={{ backgroundColor: Colors.white }}>
                <Appbar.Action icon={() => <FeatherIcon
                    size={24}
                    name="menu"
                    style={{ backgroundColor: 'transparent' }}
                    color={Colors.primaryGreen2}
                />}
                    // onPress={() => { this.props.navigation.dispatch(DrawerActions.toggleDrawer())}} 
                    onPress={() => this.props.navigation.openDrawer()}
                />
                <Appbar.Content title="We India" color={Colors.darkGray} />
                <Appbar.Action icon={() => <FeatherIcon
                    size={24}
                    name="more-vertical"
                    style={{ backgroundColor: 'transparent' }}
                    color={Colors.primaryGreen2}
                />} 
                    // onPress={() => { this.props.navigation.goBack() }} 
                    />
            </Appbar.Header>
        );
    }


    renderData() {
        return (
            <View>
                <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 8, marginBottom: 8, backgroundColor: Colors.white, borderRadius: 5, width: width * 0.98, borderColor: Colors.primaryGreen1, borderWidth: 1, height: height * 0.1 }}>
                    <Text style={{padding : 10, fontSize: 20, textAlign: 'center', color: Colors.primaryGreen1 }}> 13 cards completed in one day</Text>
                </View>

                <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 8, marginBottom: 8, backgroundColor: Colors.white, borderRadius: 5, width: width * 0.98, borderColor: Colors.primaryGreen1, borderWidth: 1, height: height * 0.1 }}>
                {/* <ScrollView> */}

                    <Text style={{padding : 10, fontSize: 20, textAlign: 'center', color: Colors.primaryGreen1 }}>
                        132134 minutes left to complete 54 cards.
                    </Text>
                {/* </ScrollView> */}
                </View>

                <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 8, marginBottom: 8, backgroundColor: Colors.white, borderRadius: 5, width: width * 0.98, borderColor: Colors.primaryGreen1, borderWidth: 1, height: height * 0.1 }}>
                    <ScrollView>

                    <Text style={{padding : 10, fontSize: 20, textAlign: 'center', color: Colors.primaryGreen1 }}>
                        HISTORY AND CULTURE subject studied 4 times than any other subject.
                    </Text>
                    </ScrollView>
                </View>
            </View>
        );
    }

    render() {
        const { showAlert } = this.state;
        return (
            <View>
                {this.renderAppBar()}
                <View style={styles.container}>
                    {this.renderData()}
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: width,
        height: height,
        justifyContent: 'center',
        flexDirection: 'column',
        alignItems: 'center'
    },
    text: {
        fontSize: 20,
        textAlign: 'center',
        padding: 10,
        color: Colors.white,
    },
    linearGradient: {
        marginTop: 10,
        marginLeft: 20,
        marginRight: 20,
        marginBottom: 10,
        elevation: 10,
        borderRadius: 30,
        // justifyContent : 'flex-end'
    },
});
