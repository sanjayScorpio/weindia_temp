/* eslint-disable react/jsx-filename-extension */
/**
 * @flow
 * @format
 */
import * as React from 'react';
import { TouchableOpacity, FlatList, Image, StyleSheet, TextInput, View } from 'react-native';
import tabBarIcon from '../components/tabBarIcon';
import {
  Button,
  Card,
  Divider,
  IconButton,
  List,
  Portal,
  Appbar,
  Text,
  Title,
  Surface
} from 'react-native-paper';
import StickyHeaderFooterScrollView from 'react-native-sticky-header-footer-scroll-view';
import mapMarker from '../constants/assets';
import { getCustomerAlternateAddress } from '../config/apiClient';
import Icon from 'react-native-vector-icons/Entypo';
import SelectAddress from './SelectAddress';
import { PrefUtils } from '../utils/PrefUtils';
import AsyncStorage from '@react-native-community/async-storage';
import LinearGradient from 'react-native-linear-gradient';
import Colors from '../styles/colors';

class LeaveStatus extends React.Component {
  // static navigationOptions = {
  //   tabBarColor: '#6200ee',
  //   tabBarIcon: tabBarIcon('cart'),
  // };
  constructor(props) {
    super(props);
    this.navigation = props.navigation;
    // this.state.cartItemsData = props.navigation.getParam('cartItems', {});
    // console.log("Cart Data:", JSON.stringify(this.state.cartItemsData))
    // this.state.storeListData = props.navigation.getParam('storeListData', {});
    // console.log("Storelist Data:", JSON.stringify(this.state.storeListData))

    this.state = {
      attendanceStatus: 'Punched In'
    };
  }

  // componentDidMount() {
  //   // retrieveData(PrefUtils.PREF_CART_ITEMS)
  //   //   .then(cartItemsData =>
  //   //     this.setState({
  //   //       cartItemsData: JSON.parse(cartItemsData),
  //   //       newCartItemList: cartItemsData.ordered_items,
  //   //     }),
  //   //   )
  //   //   .catch(error => console.error(error));
  //   // retrieveData(PrefUtils.PREF_STORE_LIST_DATA)
  //   //   .then(storeListData =>
  //   //     this.setState({storeListData: JSON.parse(storeListData)}),
  //   //   )
  //   //   .catch(error => console.error(error));
  //   let keys = [
  //     PrefUtils.PREF_CART_ITEMS,
  //     PrefUtils.PREF_STORE_LIST_DATA,
  //     PrefUtils.PREF_USER_DATA,
  //   ];
  //   AsyncStorage.multiGet(keys, (err, stores) => {
  //     stores.map((result, i, store) => {
  //       // get at each store's key/value so you can work with it
  //       let key = store[i][0];
  //       let value = JSON.parse(store[i][1]);

  //       if (key === PrefUtils.PREF_CART_ITEMS) {
  //         if (value === null || value === undefined) {
  //           value = { ordered_items: [] };
  //         }
  //         this.setState({
  //           cartItemsData: value,
  //           newCartItemList: value.ordered_items,
  //         });
  //       } else if (key === PrefUtils.PREF_STORE_LIST_DATA) {
  //         this.setState({ storeListData: value });
  //       } else if (key === PrefUtils.PREF_USER_DATA) {
  //         const { id } = value;
  //         this.setState({ cid: id });
  //         this.getSavedAddress(id);
  //       }
  //     });
  //   });

  //   console.log('====================================');
  //   console.log('Store Selected', this.state.storeListData);
  //   console.log('====================================');

  //   console.log('====================================');
  //   console.log('Store items in cart', this.state.cartItemsData);
  //   console.log('====================================');
  // }

  renderAppBar() {
    return (
      <Appbar.Header>
        <Appbar.Action icon="arrow-back" color={Colors.white} onPress={() => { this.props.navigation.goBack() }} />
        <Appbar.Content title="Attendance Details" color={Colors.white} />
      </Appbar.Header>
    );
  }

  renderAttendanceData() {
    return (
      <View style={styles.container}>
        <View style={{marginTop : 50}}>
        <TouchableOpacity
          onPress={this.handleOTPVerification}
          activeOpacity={0.9}>
          <LinearGradient colors={[Colors.primaryBlue1, Colors.primaryBlue2]}
            start={{ x: 1, y: 0 }} end={{ x: 0, y: 1 }}
            style={styles.linearGradient}>
            <Text style={styles.text}>Leave status</Text>
          </LinearGradient>
        </TouchableOpacity>
        </View>
      </View>
    )
  }

  render() {
    return (
      <View>
        {this.renderAppBar()}
        {this.renderAttendanceData()}
      </View>
    );
  }
}

const styles = StyleSheet.create({

  container: {
    justifyContent: 'center',
    flexDirection: 'column',
    margin: 10,
  },
  circleGreen: {
    width: 15,
    height: 15,
    borderRadius: 15 / 2,
    margin: 10,
    backgroundColor: Colors.green500,
  },
  circleRed: {
    width: 15,
    height: 15,
    borderRadius: 15 / 2,
    margin: 10,
    backgroundColor: Colors.red500,
  },
  surface: {
    padding: 18,
    marginTop: 8,
    marginBottom: 8,
    marginLeft: 10,
    marginRight: 10,
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 2,
    borderRadius: 8,
  },
  surfaceText: {
    fontSize: 25,
    textAlign: 'center',
    color: Colors.darkGray
  },
  text: {
    fontSize: 25,
    textAlign: 'center',
    padding: 15,
    color: Colors.white,
  },
  linearGradient: {
    margin: 8,
    elevation: 10,
    borderRadius: 50
  },
  row: {
    flexDirection: 'row',
  },
  status: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  statusText: {
    fontSize: 20,
    textAlign: 'center',
    color: Colors.darkGray
  },
  newStyle: {},
});

export default LeaveStatus;