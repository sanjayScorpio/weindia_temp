/* eslint-disable react/jsx-filename-extension */
/**
 * @flow
 * @format
 */
import * as React from 'react';
import { TouchableOpacity, FlatList, Image, StyleSheet, TextInput, View, Dimensions } from 'react-native';
import {
  Button,
  Card,
  Divider,
  IconButton,
  List,
  Portal,
  Dialog
} from 'react-native-paper';
import { Avatar, ListItem, Rating, AirbnbRating } from 'react-native-elements';
import Icon from 'react-native-vector-icons/Entypo';
import { PrefUtils } from '../utils/PrefUtils';
import AsyncStorage from '@react-native-community/async-storage';
import { RNToasty } from 'react-native-toasty';
import LinearGradient from 'react-native-linear-gradient';
import Colors from '../styles/colors';
import { ScrollView } from 'react-native-gesture-handler';
import agri from '../../assets/images/agriculture.jpg'

const { width, height } = Dimensions.get('window');

class  SubCardDetailsScreen  extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      dialogVisible : false
    }
  }

  setDialogVisible = () =>
        this.setState({ dialogVisible: !this.state.dialogVisible });


        onPressRatings = () => {
          return (
              <Portal>
                  <Dialog
                      visible={this.state.dialogVisible}
                      onDismiss={this.setDialogVisible}>
                      <Dialog.Title style={styles.centerTitle}>
                          How was your experience with us?
                </Dialog.Title>
                      <Dialog.Content>
                          <View>
                              <AirbnbRating
                                  color={Colors.primary}
                                  count={5}
                                  reviews={['🤮 Terrible  ', '😐 OK  ', '😊 Good  ', '😀 wow  ', '😍 Amazing  ']}
                                  defaultRating={4}
                                  size={30}
                              />
                          </View>
                      </Dialog.Content>
                      <Dialog.Actions>
                          <Button
                              style={styles.m_10}
                              mode="text"
                              onPress={this.setDialogVisible}>
                              Done
                  </Button>
                          <Button
                              style={styles.m_10}
                              mode="text"
                              onPress={this.setDialogVisible}>
                              Cancel
                  </Button>
                      </Dialog.Actions>
                  </Dialog>
              </Portal>
          );
      };
  


  render(){
    return(
      <View>

      </View>
    );
  }
}
const styles = StyleSheet.create({

  container: {
    // flex: 1,
    justifyContent: 'space-between',
    flexDirection: 'column',
    alignItems: 'center'
  },
  linearGradient: {
    elevation: 5,
    borderRadius: 30,
    width : width * 0.25,
    height : height * 0.055,
    marginLeft : 5,
    marginRight : 5,
  },
  text: {
    fontSize: 16,
    textAlign: 'center',
    paddingTop: 5,
    color: Colors.white,
    marginBottom : -2
},
text2: {
  fontSize: 10,
  textAlign: 'center',
  color: Colors.white,
},

  //  top fixed Card
  fixedCardContainer: {
    justifyContent: 'center',
    backgroundColor: Colors.white,
    width: width,
    height: height * 0.1,
    borderColor: Colors.primaryGreen2,
    borderWidth: 1,
    borderRadius: 5,
    marginTop: 3,
  },
  innerRow: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  cardFlex: {
    flexDirection: 'column',
    flex: 0.4,
    justifyContent: 'space-around'
  },
  cardDivider: {

  },
  mainText: {
    color: Colors.primaryGreen2,
    fontSize: 12
  },
  subText: {},

  // render Card content
  baseCard: {
    alignItems: 'center',
    justifyContent: 'center',
    width: width,
    height: height * 0.75,
    backgroundColor: Colors.white,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    elevation: 20,
  },

  // Question Card
  questionCard: {
    height: height * 0.1,
    width: width * 0.9,
    // backgroundColor : Colors.white,
    flexDirection: 'column',
    justifyContent: 'space-around'
  }


}
);

export default SubCardDetailsScreen;