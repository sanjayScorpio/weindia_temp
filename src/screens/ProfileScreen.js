import React, { Component } from 'react';
import { StyleSheet, TouchableOpacity, View, Dimensions, Image } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import AwesomeAlert from 'react-native-awesome-alerts';
import { PrefUtils } from '../utils/PrefUtils';
import { Appbar, Text, Divider } from 'react-native-paper';
import Colors from '../styles/colors';
import FeatherIcon from 'react-native-vector-icons/Feather';
import LinearGradient from 'react-native-linear-gradient';
import { Title } from 'react-native-paper';
import user from "../../assets/images/user.jpg";
import { ScrollView } from 'react-native-gesture-handler';

const { width, height } = Dimensions.get('window');

export default class Profile extends Component {

    constructor(props) {
        super(props);
        this.state = {
            firstname: 'Your',
            lastname: 'Name',
            fullname: '',
            adress: 'Vijaynagar 1st stage Mysore',
            useremail: 'NA',
            phonenumber: 'NA',
            showAlert: false,
        };
    }


    showAlert = () => {
        this.setState({
            showAlert: true,
        });
    };

    hideAlertWithSignout = () => {
        this.setState({
            showAlert: false,
            headerMode: 'screen',
        });
        this.logoutUser();
        this.props.navigation.navigate('SignIn');
    };

    hideAlertWithoutSignout = () => {
        this.setState({
            showAlert: false,
            headerMode: 'screen',
        });
    };

    // componentDidMount() {        
    //     this._retrieveData(PrefUtils.PREF_USER_EMAIL);
    //     this._retrieveData(PrefUtils.PREF_USER_MOBILE);
    //     this._retrieveData(PrefUtils.PREF_USER_FULLNAME);
    // }

    logoutUser = () => {
        this.removeData(PrefUtils.PREF_USER_EMAIL);
        this.removeData(PrefUtils.PREF_USER_FNAME);
        this.removeData(PrefUtils.PREF_USER_FNAME);
        this.removeData(PrefUtils.PREF_USER_FULLNAME);
        this.removeData(PrefUtils.PREF_USER_ID);
        this.removeData(PrefUtils.PREF_USER_MOBILE);
    };

    // _retrieveData = async key => {
    //     try {
    //         const value = await AsyncStorage.getItem(key);
    //         console.log(key + 'sd => ' + value);
    //         if (value !== null) {
    //             // We have data!!
    //             if (key === PrefUtils.PREF_USER_EMAIL) {
    //                 this.setState({
    //                     useremail: value,
    //                 });
    //             } else if (key === PrefUtils.PREF_USER_MOBILE) {
    //                 this.setState({
    //                     phonenumber: value,
    //                 });

    //             } else if (key === PrefUtils.PREF_USER_FULLNAME) {
    //                 this.setState({
    //                     fullname: value,
    //                 });

    //             }
    //             return value;
    //         }
    //     } catch (error) {
    //         // Error retrieving data
    //         console.log(error.message);
    //     }
    // };

    renderContentData = () => (
        <View style={[styles.AlignCenter, { justifyContent: 'space-between' }]}>

            <View style={[styles.AlignCenter, { marginBottom: 20 }]}>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <FeatherIcon
                        size={18}
                        name="users"
                        style={{ backgroundColor: 'transparent' }}
                        color={Colors.primaryBlue2}
                    />
                    <Text style={styles.contentTextHeader}>Role</Text>
                </View>
                <Text style={styles.contentTextContent}>Rider</Text>
            </View>

            <View style={[styles.AlignCenter, { marginBottom: 20 }]}>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <FeatherIcon
                        size={18}
                        name="phone"
                        style={{ backgroundColor: 'transparent' }}
                        color={Colors.primaryBlue2}
                    />
                    <Text style={styles.contentTextHeader}>Phone</Text>
                </View>
                <Text style={styles.contentTextContent}>+91 9652125478</Text>
            </View>

            <View style={[styles.AlignCenter, { marginBottom: 20 }]}>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <FeatherIcon
                        size={18}
                        name="mail"
                        style={{ backgroundColor: 'transparent' }}
                        color={Colors.primaryBlue2}
                    />
                    <Text style={styles.contentTextHeader}>Email</Text>
                </View>
                <Text style={styles.contentTextContent}>ashly@gmail.com</Text>
            </View>

            <View style={[styles.AlignCenter, { marginBottom: 20 }]}>
                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                    <FeatherIcon
                        size={18}
                        name="map-pin"
                        style={{ backgroundColor: 'transparent' }}
                        color={Colors.primaryBlue2}
                    />
                    <Text style={styles.contentTextHeader}>Address</Text>
                </View>
                <Text style={[styles.contentTextContent, { flexShrink: 1 }]}>4th cross, saraswathipuram </Text>
            </View>

        </View>
    );

    renderTopContent = () => (
        <View>
            <Image source={user}
                resizeMode='cover'
                style={styles.image}
            />
            <Title style={styles.p_name}>Ashly Jackson</Title>
            <Title style={styles.p_subName}>Marketing Head, FilterQuartz</Title>
            <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                <View style={[styles.iconCircle, { backgroundColor: Colors.primaryBlue2 }]}>
                    <FeatherIcon
                        size={24}
                        name="facebook"
                        style={{ backgroundColor: 'transparent' }}
                        color={Colors.white}
                    />
                </View>
                <View style={[styles.iconCircle, { backgroundColor: Colors.red500 }]}>
                    <FeatherIcon
                        size={24}
                        name="instagram"
                        style={{ backgroundColor: 'transparent' }}
                        color={Colors.white}
                    />
                </View>
                <View style={[styles.iconCircle, { backgroundColor: Colors.primaryBlue1 }]}>
                    <FeatherIcon
                        size={24}
                        name="twitter"
                        style={{ backgroundColor: 'transparent' }}
                        color={Colors.white}
                    />
                </View>
            </View>
        </View>
    )

    removeData = async key => {
        try {
            const value = await AsyncStorage.removeItem(key);
            console.log(key + 'sd => ' + value);
            if (value !== null) {
                // We have data!!
                return value;
            }
        } catch (error) {
            // Error retrieving data
            console.log(error.message);
        }
    };

    renderAppBar() {
        return (
            <Appbar.Header>
                <Appbar.Action icon="arrow-back" onPress={() => {
                    this.props.navigation.goBack();
                }} />
                <Appbar.Content title="Account" />
            </Appbar.Header>
        );
    }

    render() {
        const { showAlert } = this.state;
        return (
            <View>
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <LinearGradient colors={[Colors.primaryBlue1, Colors.primaryBlue2]}
                        start={{ x: 0, y: 0 }} end={{ x: 0, y: 1 }}>
                        <ScrollView>
                            <View style={styles.container}>
                                <View style={styles.CardContainer}>
                                    {this.renderTopContent()}
                                    <View>
                                        <Divider />
                                        <Divider />
                                    </View>
                                    {this.renderContentData()}
                                    <View>
                                        <TouchableOpacity
                                            onPress={() => this.showAlert()}
                                            activeOpacity={0.8}>
                                            <LinearGradient colors={[Colors.primaryBlue1, Colors.primaryBlue2]}
                                                start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}
                                                style={styles.linearGradient}>
                                                <Text style={styles.text}>Logout</Text>
                                            </LinearGradient>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                        </ScrollView>
                    </LinearGradient>
                </View>


                <AwesomeAlert style={this.state.contentContainerStyle}
                    show={showAlert}
                    showProgress={false}
                    title="Log Out!"
                    message="Are sure you want to logout?"
                    closeOnTouchOutside={true}
                    closeOnHardwareBackPress={false}
                    showCancelButton={true}
                    showConfirmButton={true}
                    cancelText="No, cancel"
                    confirmText="Yes, logout!"
                    confirmButtonColor="#DD6B55"
                    onCancelPressed={() => {
                        this.hideAlertWithoutSignout();
                    }}
                    onConfirmPressed={() => {
                        this.hideAlertWithSignout();
                    }}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: width,
        height: height,
        justifyContent: 'center',
        // flexDirection : 'column'
        // alignItems : 'center'
    },
    AlignCenter: {
        alignItems: 'center'
    },
    text: {
        fontSize: 20,
        textAlign: 'center',
        padding: 10,
        color: Colors.white,
    },
    linearGradient: {
        marginTop: 10,
        marginLeft: 20,
        marginRight: 20,
        marginBottom: 10,
        elevation: 10,
        borderRadius: 30,
        // justifyContent : 'flex-end'
    },
    CardContainer: {
        width: width * 0.9,
        height: height * 0.9,
        backgroundColor: Colors.white,
        alignSelf: 'center',
        borderRadius: 10,
        justifyContent: "space-between",
        flexDirection: 'column'
    },
    image: {
        width: 124,
        height: 124,
        borderRadius: 64,
        alignSelf: 'center',
        marginTop: 10,
        marginBottom: 10,
    },
    p_name: {
        marginTop : 10,
        fontSize: 25,
        textAlign: 'center',
        color: Colors.darkGray
    },
    p_subName: {
        fontSize: 18,
        textAlign: 'center',
        color: Colors.lightGray
    },
    iconCircle: {
        width: 50,
        height: 50,
        borderRadius: 50 / 2,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 5
    },
    contentTextHeader: {
        color: Colors.lightGray,
        fontSize: 18,
        marginTop: 3,
        marginBottom: 5,
        margin: 5,
        textAlign: 'center',
        justifyContent: 'center',
        alignItems: 'center'
    },
    contentTextContent: {
        color: Colors.darkGray,
        fontSize: 20,
        margin: 3,
        textAlign: 'center',
    }
});
