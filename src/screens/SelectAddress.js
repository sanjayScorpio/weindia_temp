/**
 * @flow
 * @format
 */

import * as React from 'react';
import {FlatList, StyleSheet, View} from 'react-native';
import {Divider, List} from 'react-native-paper';
import Colors from '../styles/colors';

const isToggle = {isSelected: true};

class SelectAddress extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      custAddressData: this.props.data,
    };
  }

  itemSelected = item => {
    this.props.OnItemSelected(item);
  };

  _keyExtractor = item => item.id.toString();

  renderList = custAddressData => (
    <FlatList
      contentContainerStyle={{backgroundColor: 'transparent'}}
      ItemSeparatorComponent={Divider}
      renderItem={this._renderItem}
      keyExtractor={this._keyExtractor}
      data={custAddressData}
      extraData={this.props.refresh}
      style={this.props.styles === undefined ? {} : this.props.styles}
      ListFooterComponent={() => <View style={{height: 150}} />}
    />
  );

  _renderItem = ({item}) => (
    <List.Item
      title={item.address_type}
      description={item.address}
      left={props => {
        const type = item.address_type;

        if (type === 'work') {
          return <List.Icon {...props} icon="work" />;
        } else if (type === 'home') {
          return <List.Icon {...props} icon="home" />;
        } else {
          return <List.Icon {...props} icon="person_pin_circle" />;
        }
      }}
      onPress={() => {
        this.itemSelected(item);
      }}
    />
  );

  render() {
    const custAddressData = this.props.data;
    //console.warn(JSON.stringify(custAddressData));
    return <View>{this.renderList(custAddressData)}</View>;
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default SelectAddress;
