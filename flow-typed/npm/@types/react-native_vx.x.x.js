// flow-typed signature: 80fd9c475cd03fb773ecb72bdaac537a
// flow-typed version: <<STUB>>/@types/react-native_v^0.57.7/flow_v0.94.0

/**
 * This is an autogenerated libdef stub for:
 *
 *   '@types/react-native'
 *
 * Fill this stub out by replacing all the `any` types.
 *
 * Once filled out, we encourage you to share your work with the
 * community by sending a pull request to:
 * https://github.com/flowtype/flow-typed
 */

declare module '@types/react-native' {
  declare module.exports: any;
}
