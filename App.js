/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
/**
 * @format
 * @flow
 */

import React, {Component} from 'react';
//import SplashScreen from 'react-native-splash-screen'
import {Provider as PaperProvider, DefaultTheme} from 'react-native-paper';
import Navigations from './src/navigations';
//import LogConfig from './ReactotronConfig';
//import Config from 'react-native-config';


const theme = {
  ...DefaultTheme,
  roundness: 2,
  colors: {
    ...DefaultTheme.colors,
    primary: '#268E4F',
    accent: '#175F36',
  },
  fonts : {
    regular : 'semibold',
    thin : 'light',
    medium : 'bold',
    light : 'regular'
  }

};

export default class App extends Component{
  componentDidMount() {
    //SplashScreen.hide();
    // if (__DEV__) {
    //   LogConfig.configure({enableLog: true});
    // }
    //console.log('Config Values =>', Config.API_URL);
  }

  render() {
    return (
      <PaperProvider theme = {theme}>
        <Navigations/>
      </PaperProvider>
    );
  }
}
