import pin from './images/icons/pin.png';
import pulse from './anim/pulse';
import store_items from './images/store-items.jpg';
import map_bg from './images/map-bg.jpg';
import anim_location_marker_loader from './anim/location_marker_loader';
import ic_suitcase_outline_24px from './images/icons/ic_suitcase_outline_24px.png';
import ic_marker_position from './images/icons/position.png';
import ic_marker_position_32px from './images/icons/ic_marker_position_32px.png'
import giffy_logo from './images/giffy_logo_512px.png'
export {
  pin,
  pulse,
  store_items,
  map_bg,
  anim_location_marker_loader,
  ic_suitcase_outline_24px,
  ic_marker_position,
  ic_marker_position_32px,
  giffy_logo
};
